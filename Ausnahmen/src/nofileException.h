/*
 * nofileException.h
 *
 *  Created on: 13.03.2019
 *  Author: sefi16
 */

#ifndef NOFILEEXCEPTION_H
#define NOFILEEXCEPTION_H

#include <exception>

class nofileException : public std::exception {

	public:
		virtual const char* what() const throw() {
			return "Die Datei existiert nicht";
		}
};

#endif /* NOFILEEXCEPTION_H */
