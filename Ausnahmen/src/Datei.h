/*
 * Datei.h
 *
 *  Created on: 13.03.2019
 *  Author: sefi16
 */

#ifndef DATEI_H
#define DATEI_H

#include <string>
#include "nofileException.h"

class Datei {
	private:

	public:
		Datei();
		~Datei();
		bool read_file(std::string Dateiname);
		bool write_file(std::string Dateiname);
};

#endif /* DATEI_H */
