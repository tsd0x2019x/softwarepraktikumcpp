/*
 * main.cpp
 *
 *  Created on: 13.03.2019
 *  Author: sefi16
 */
#include <iostream>
#include "Datei.h"

int main() {

	Datei datei = Datei();

	std::string sPath = "/mnt/hgfs/Z/Dateien/";
	std::string sFilename = "";

	std::cout << "Bitte einen Dateiname eingeben: ";
	std::cin >> sFilename;

	sPath += sFilename;

	datei.read_file(sPath);
	std::cout << std::endl;

	datei.write_file(sPath);
	std::cout << std::endl;

	datei.read_file(sPath);

	return 0;
}

