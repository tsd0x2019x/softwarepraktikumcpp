/*
 * Datei.cpp
 *
 *  Created on: 13.03.2019
 *  Author: sefi16
 */
#include <iostream>
#include <string>
#include <fstream>
#include "Datei.h"

Datei::Datei() {}

Datei::~Datei() {}

bool Datei::read_file(std::string Dateiname) {
	std::ifstream iFile; // Filestream mit Lesezugriff
	iFile.open(Dateiname.c_str());
	if (!iFile.is_open()) return false; // Check if file can be opened
	std::string sLine = "";
	while(!iFile.eof()) { // Solange Dateiende noch nicht erreicht ist
		std::getline(iFile, sLine); // Lese eine Zeile from iFile nach sLine
		std::cout << sLine << std::endl;
	}
	iFile.close();
	return true;
}

bool Datei::write_file(std::string Dateiname) {
	std::ofstream oFile; // Filestream mit Schreibzugriff
	oFile.open(Dateiname.c_str(), std::ios::out | std::ios::ate | std::ios::app); // Setzt Cursor ans Dateiende (ate) und Text am Ende anhängen (app)
	if (!oFile.is_open()) return false;
	std::string sLine = "";
	while (sLine.compare("exit") != 0) {
		std::cout << "Text eingeben und ENTER drücken (exit zum Beenden): ";
		std::cin >> sLine; // Einlesen in sLine
		if (sLine.compare("exit") != 0)
			oFile << sLine << std::endl;
	};
	std::cout << "Beendet." << std::endl;
	oFile.close();
	return true;
}
