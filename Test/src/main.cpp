#include <iostream>

class Bass
{
public:
    virtual void droehn() { std::cout << "Bass" << std::endl; }
};

class Tuba : public Bass
{
public:
    void droehn() { std::cout << "Tuba" << std::endl; }
};

void MachMalTut(Bass *tute)
{
    tute->droehn();
}

int main()
{
    Tuba tuba;
    Bass bass;

    MachMalTut(&bass);
    MachMalTut(&tuba);

    return 0;
}
