//============================================================================
// Name        : Palindrome.cpp
// Author      : sefi16
// Version     :
// Copyright   : Your copyright notice
// Description : Palindrome in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
using namespace std;

bool checkPalindrom (string sWord) {
	// Laenge des Wortes
	int iWordLength = sWord.size();
	// Stelle an der Worthälfte
	int iPosHalf = ( iWordLength % 2 ) ? iWordLength / 2 : iWordLength / 2 - 1;

	// Check Palindrom
	for (int i = 0; i <= iPosHalf; i++) {
		if (sWord[i] != sWord[iWordLength-i-1]) return false;
	}

	return true;
}

int main() {
	cout << "*** Palindrome ***" << endl;

	string sWord = "";

	cout << "Bitte ein Wort eingeben" << endl;
	cin >> sWord;

	while (cin.fail()) {
		cout << "ERROR::Falsche Eingabe: Bitte ein Wort eingeben" << endl;
		cin.clear();
		cin.ignore(200, '\n');
		cin >> sWord;
	}

	if ( checkPalindrom(sWord) ) {
		cout << "[" << sWord << "] ist ein Palindrom" << endl;
	}
	else {
		cout << "[" << sWord << "] ist KEIN Palindrom" << endl;
	}

	return 0;
}
