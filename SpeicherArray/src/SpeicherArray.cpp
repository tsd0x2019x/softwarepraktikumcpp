//============================================================================
// Name        : SpeicherArray.cpp
// Author      : sefi16
// Version     :
// Copyright   : Your copyright notice
// Description : Speicher und Arrays in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main() {
	cout << "!*** Speicher und Arrays ***" << endl;

	/**
	 * a) int iStack[100000000];
	 * 	  cout << "Ausgabe" << endl;
	 *
	 * Dieses int-Array hat sehr große Dimension und würde den gesamten Speicherbereich beanspruchen.
	 * Folgende Answeisungen, z.B. cout << "Ausgabe" << endl;, können nicht mehr ausgeführt werden, da nicht genug Speicher.
	 */

	/**
	 * b) Array mit 100000 Elementen und Zufallszahlen
	 */
	int iArrSize = 100000;
	int* iStack = new int[iArrSize];
	int iCounter = 0;

	srand(time(NULL)); // Einmalige Initialisierung des Zufallsgenerators

	// Füllt Array mit Zufallszahlen zwischen 0 und 100. Und prüfe, wie viele der erzeugten Zufallszahlen ohne Rest durch 13 teilbar sind.
	for (int i = 0; i < iArrSize; i++) {
		iStack[i] = rand() % 101;
		cout << "iStack[" << i << "] : " << iStack[i] << endl;
		if (iStack[i] % 13 == 0)  iCounter += 1;
	}

	cout << "Anzahl der Zufallszahlen ohne Rest durch 13 teilbar: " << iCounter << endl;

	// Speicherplatz des Arrays freigeben
	delete[] iStack;

	return 0;
}
