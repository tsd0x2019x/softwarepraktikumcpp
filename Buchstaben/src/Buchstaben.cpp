//============================================================================
// Name        : Buchstaben.cpp
// Author      : sefi16
// Version     :
// Copyright   : Your copyright notice
// Description : Buchstaben und Wörter in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
using namespace std;


/**
 * Diese Methode erstellt zuerst eine (Shadow) Kopie des ursprünglichen Strings. Dann entfernt sie alle vorhandenen
 * Vokale (a,e,i,o,u) in der String-Kopie und gibt diese veränderte Kopie zurück.
 *
 * A (66)
 * E (69)
 * I (73)
 * O (79)
 * U (85)
 * a (97)
 * e (101)
 * i (105)
 * o (111)
 * u (117)
 */
string RemoveVocal(string str) {
	int iWordLength = str.size();
	string newString = "";

	for (int i = 0; i < iWordLength; i++) {
		char ch = str[i];
		if ( !(ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' || ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U') ) {
			//newString.append(&ch);
			newString.append(str.substr(i,1));
		}
	}
	return newString;
}

/**
 * Diese Methode verändert den String direkt.
 */
void ModifyString2(string * pstr) {
	int iWordLength = (*pstr).size();
	int i = 1;
	char cTemp = '0';

	while ( i <= iWordLength-3 ) {
		cTemp = (*pstr)[i];
		(*pstr)[i] = (*pstr)[i+1];
		(*pstr)[i+1] = cTemp;
		i += 2;
	}
}

/**
 * Diese Methode erstellt zuerst eine (Shadow) Kopie des zugewiesenen String-Objekts, verändert die Kopie und gitb sie zurück.
 * Das usprüngliche String-Objekt bleibt unverändert (immun).
 *
 * Rückgabewert ist die (veränderte) Kopie des ursprünglichen String-Objekts.
 */
string ModifyString(string str) {
	int iWordLength = str.size();
	int i = 1;
	char cTemp = '0';

	while ( i <= iWordLength-3 ) {
		cTemp = str[i];
		str[i] = str[i+1];
		str[i+1] = cTemp;
		i += 2;
	}

	return str;
}

int main() {
	cout << "*** Buchstaben und Wörter" << endl;

	string sWord = "";
	string sWord2 = "";

	cout << "Bitte ein Wort eingeben" << endl;
	cin >> sWord;

	while (cin.fail()) {
		cout << "ERROR::Falscheingabe: Bitte ein Wort eingeben" << endl;
		cin.clear();
		cin.ignore(200, '\n');
		cin >> sWord;
	}

	cout << "Eingegebenes Wort: " << sWord << endl;

	//ModifyString2(&sWord);
	//cout << "Verändertes Wort: " << sWord << endl;
	sWord2 = ModifyString(sWord);
	cout << "Verändertes Wort (Verkehrte Buchstaben): " << sWord2 << endl;

	sWord2 = RemoveVocal(sWord);
	cout << "Verändertes Wort (Ohne Vokale): " << sWord2 << endl;

	return 0;
}
