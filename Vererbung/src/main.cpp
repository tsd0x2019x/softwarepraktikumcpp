/*
 * main.cpp
 *
 *  Created on: 12.03.2019
 *  Author: sefi16
 */
#include <iostream>
#include "Student.h"

int main() {
	Person P1 = Person("Seehofer", "Horst", 69);
	Person P2 = Student("Trump", "Barron", 17, 3204857);

	std::cout << "Person 1: " << P1.getName() << ", " << P1.getAge() << " Jahre alt" << std::endl;
	std::cout << "Person 2: " << P2.getName() << ", " << P2.getAge() << " Jahre alt" << std::endl;

	Student P3 = Student("Trump", "Barron", 17, 3204857);

	std::cout << "Person 3: " << P3.getName() << ", " << P3.getAge() << " Jahre alt" << std::endl;

	return 0;
}



