/*
 * Person.h
 *
 *  Created on: 12.03.2019
 *  Author: sefi16
 */

#ifndef PERSON_H
#define PERSON_H

#include <string>

class Person
{
	private:
		std::string m_sName;
		std::string m_sSurname;
		int m_iAge;

	public:
		Person(std::string sName, std::string sSurname, int iAge);
		~Person();
		std::string getName();
		std::string getSurname();
		int getAge();
		bool setAge(int iAge);

};

#endif /* PERSON_H */



