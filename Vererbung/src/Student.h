/*
 * Student.h
 *
 *  Created on: 12.03.2019
 *  Author: sefi16
 */

#ifndef STUDENT_H
#define STUDENT_H

#include "Person.h"

class Student : public Person
{

	private:
		unsigned int m_uiStudentID;

	public:
		Student(std::string sName, std::string sSurname, int iAge, unsigned int uiStudentID);
		~Student();
		unsigned int getStudentID();
		std::string getName(); // Override method

};

#endif /* STUDENT_H */
