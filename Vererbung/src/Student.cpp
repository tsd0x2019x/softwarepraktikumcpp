/*
 * Student.cpp
 *
 *  Created on: 12.03.2019
 *  Author: sefi16
 */
#include "Student.h"

// Standardkonstruktor mit Vererbung
//Student::Student(std::string sName, std::string sSurname, int iAge, unsigned int uiStudentID) {
//	Person(sName, sSurname, iAge);
//	m_uiStudentID = uiStudentID;
//}

// Standardkonstruktor mit Vererbung und Initialisierungsliste
Student::Student(std::string sName, std::string sSurname, int iAge, unsigned int uiStudentID) :
Person(sName, sSurname, iAge), m_uiStudentID(uiStudentID)
{}

// Standarddestruktor
Student::~Student() {}

unsigned int Student::getStudentID() {
	return m_uiStudentID;
}

std::string Student::getName() { // Override method
	return "Student " + Person::getName();
}




