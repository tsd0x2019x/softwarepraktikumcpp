/*
 * Person.cpp
 *
 *  Created on: 12.03.2019
 *  Author: sefi16
 */
#include "Person.h"

// Standardkonstruktor
//Person::Person(std::string sName, std::string sSurname, int iAge) {
//	m_sName = sName;
//	m_sSurname = sSurname;
//	m_iAge = iAge;
//}

// Standardkonstruktor mit Initialisierungsliste
Person::Person(std::string sName, std::string sSurname, int iAge) : m_sName(sName), m_sSurname(sSurname), m_iAge(iAge) {}

// Standarddestruktor
Person::~Person() {}

std::string Person::getName() {
	return m_sName;
}

std::string Person::getSurname() {
	return m_sSurname;
}

int Person::getAge() {
	return m_iAge;
}

bool Person::setAge(int iAge) {
	if (iAge >= 0) {
		m_iAge = iAge;
		return true;
	}
	return false;
}



