/*
 * Flaschen.h
 *
 *  Created on: 11.03.2019
 *  Author: sefi16
 */

#ifndef FLASCHE_H
#define FLASCHE_H

#include <string>

class Flasche
{
	private:
		double dVolumen;
		std::string sMaterial;

	public:
		Flasche();
		Flasche(double volumen, std::string material);
		~Flasche();
		double getVolumen();
		std::string getMaterial();
		void setVolumen(double volumen);
		void setMaterial(std::string material);
		void printFlasche();
		void adoptFlasche(Flasche flasche2);
};

#endif /* FLASCHE_H */
