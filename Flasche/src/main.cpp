/*
 * main.cpp
 *
 *  Created on: 11.03.2019
 *  Author: sefi16
 */
#include <iostream>
#include "Flasche.h"

int main() {

	Flasche flasche1 = Flasche();
	std::cout << "Flasche 1 wurde erstellt." << std::endl;
	flasche1.printFlasche();
	std::cout << std::endl;

	Flasche flasche2 = Flasche(100, "Glas");
	std::cout << "Flasche 2 wurde erstellt." << std::endl;
	flasche2.printFlasche();
	std::cout << std::endl;

	std::cout << "Flasche 1 Inhalte wurden aktualisiert." << std::endl;
	flasche1.setVolumen(50);
	flasche1.setMaterial("Kunststoff");
	flasche1.printFlasche();
	std::cout << std::endl;

	std::cout << "Flasche 1 adoptiert Inhalte der Flasche 2." << std::endl;
	flasche1.adoptFlasche(flasche2);

	std::cout << "Flasche 1 Inhalte jetzt." << std::endl;
	flasche1.printFlasche();
	std::cout << std::endl;

	return 0;
}



