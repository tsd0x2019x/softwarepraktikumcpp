/*
 * Flasche.cpp
 *
 *  Created on: 11.03.2019
 *  Author: sefi16
 */
#include <iostream>
#include <string>

#include "Flasche.h"

Flasche::Flasche() : dVolumen(0), sMaterial("")
{}

Flasche::Flasche(double volumen, std::string material)
{
	dVolumen = volumen;
	sMaterial = material;
}

Flasche::~Flasche() {}

double Flasche::getVolumen()
{
	return dVolumen;
}

std::string Flasche::getMaterial()
{
	return sMaterial;
}

void Flasche::printFlasche()
{
	std::cout << "=> Volumen: " << dVolumen << std::endl;
	std::cout << "=> Material: " << sMaterial << std::endl;
}

void Flasche::setVolumen(double volumen)
{
	dVolumen = volumen;
}

void Flasche::setMaterial(std::string material)
{
	sMaterial = material;
}

void Flasche::adoptFlasche(Flasche flasche2)
{
	dVolumen = flasche2.getVolumen();
	sMaterial = flasche2.getMaterial();
}
