/*
 * PossibleMove.cpp
 *
 *  Created on: 17.03.2019
 *  Author: sefi16
 */
#include "PossibleMoves.h"
#include "IndexOutOfRangeException.h"

PossibleMoves::PossibleMoves() : m_list(), iter(m_list.begin()) {}

//PossibleMoves::~PossibleMoves() {
//	m_list.clear();
//}

int PossibleMoves::GetNumberOfMoves() {
	return (int)m_list.size();
}

Coord PossibleMoves::GetMoveFromIndex(int index) { // index 0..size()-1
	if (index >= (int)m_list.size() || index < 0) throw IndexOutOfRangeException();
	iter = m_list.begin();
	for(int i = 0; i < index; i++) { iter++; }; // Erhöht iterator um 1
	return (Coord)(*iter);
}

Coord PossibleMoves::PopLastMove() {
	Coord co = m_list.back();
	m_list.pop_back();
	return co;
}


