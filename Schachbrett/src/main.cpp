/*
 * main.cpp
 *
 *  Created on: 14.03.2019
 *  Author: sefi16
 */
#include <iostream>
#include <list>
#include "Schachbrett.h"
#include "PossibleMoves.h"
#include "Coord.h"
#include "IndexOutOfRangeException.h"
#include "Springerproblem.h"

int main() {

	Springerproblem Springer = Springerproblem();
	Springer.InitializeChessboard(8);

	if (Springer.FindPathFrom(4,0)) {
		std::cout << "Path gefunden" << std::endl;
	}

	Springer.PrintPathList();

	return 0;
}



