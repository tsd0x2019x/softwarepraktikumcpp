/*
 * Springerproblem.cpp
 *
 *  Created on: 15.03.2019
 *  Author: sefi16
 */

#include <iostream>
#include "Springerproblem.h"
#include "PossibleMoves.h"


Springerproblem::Springerproblem() {
	m_schachbrett = Schachbrett();
}

Springerproblem::~Springerproblem() {
	m_pathList.clear();
	m_possibleMovesList.clear();
}

void Springerproblem::InitializeChessboard(int n) {
	m_schachbrett.Initialize(n);
}

void Springerproblem::PrintChessboard() {
	m_schachbrett.PrintBoard();
	std::cout << std::endl;
}

void Springerproblem::PrintPathList() {
	if (m_pathList.size() == 0) return;
	std::cout << std::endl << "Path length: " << ((int)m_pathList.size()) << std::endl;
	for(std::list<Coord>::iterator ite = m_pathList.begin(); ite != m_pathList.end(); ite++) {
		std::cout << "(" << ite->row << "," << ite->col << ") ";
	}
	std::cout << std::endl;
}

bool Springerproblem::IsValidField(int iRow, int iCol) {
	if (!m_schachbrett.IsValidField(iRow,iCol)) return false;
	if (!m_schachbrett.IsFreeField(iRow,iCol)) return false;
	return true;
}

/*
 *
 */
bool Springerproblem::FindPathFrom(int iRow, int iCol) {

	PrintChessboard();

	m_pathList.push_back(Coord(iRow,iCol));
	m_schachbrett.SetField(iRow,iCol);

	// Create a list of possible moves
	PossibleMoves pm;

	// 1st possible move
	if (Springerproblem::IsValidField(iRow-2,iCol-1)) pm.m_list.push_back(Coord(iRow-2,iCol-1));
	// 2nd possible move
	if (Springerproblem::IsValidField(iRow-1,iCol-2)) pm.m_list.push_back(Coord(iRow-1,iCol-2));
	// 3rd possible move
	if (Springerproblem::IsValidField(iRow+1,iCol-2)) pm.m_list.push_back(Coord(iRow+1,iCol-2));
	// 4th possible move
	if (Springerproblem::IsValidField(iRow+2,iCol-1)) pm.m_list.push_back(Coord(iRow+2,iCol-1));
	// 5th possible move
	if (Springerproblem::IsValidField(iRow+2,iCol+1)) pm.m_list.push_back(Coord(iRow+2,iCol+1));
	// 6th possible move
	if (Springerproblem::IsValidField(iRow+1,iCol+2)) pm.m_list.push_back(Coord(iRow+1,iCol+2));
	// 7th possible move
	if (Springerproblem::IsValidField(iRow-1,iCol+2)) pm.m_list.push_back(Coord(iRow-1,iCol+2));
	// 8th possible move
	if (Springerproblem::IsValidField(iRow-2,iCol+1)) pm.m_list.push_back(Coord(iRow-2,iCol+1));

	if (pm.GetNumberOfMoves() > 0) {
		//m_pathList.push_back(Coord(iRow,iCol));
		//m_schachbrett.SetField(iRow,iCol);
		Coord co = pm.PopLastMove();
		m_possibleMovesList.push_back(pm);
		return FindPathFrom(co.row,co.col);
	}
	else { // pm.GetNumberOfMoves() == 0
		if (((int)m_pathList.size()) == (m_schachbrett.GetSize() * m_schachbrett.GetSize())) {
			//m_pathList.push_back(Coord(iRow,iCol));
			//m_schachbrett.SetField(iRow,iCol);
			return true; // Springerproblem gelöst! Es gibt eine Lösung.
		}
		else { // Sackgasse => Ein Zug zurück
			if (m_possibleMovesList.size() > 0) {
				m_pathList.pop_back();
				m_schachbrett.UnsetField(iRow,iCol);
				pm = m_possibleMovesList.back();
				m_possibleMovesList.pop_back();
				Coord co;
				if (pm.GetNumberOfMoves() == 0) {
					co = m_pathList.back();
					m_schachbrett.UnsetField(co.row,co.col);
					m_pathList.pop_back();
					pm = m_possibleMovesList.back();
					m_possibleMovesList.pop_back();
				}
				co = pm.PopLastMove();
				m_possibleMovesList.push_back(pm);
				return FindPathFrom(co.row,co.col);
			}
			else {
				return false;
			}
		}
	}
	return false; // Es gibt KEINE Lösung
}


