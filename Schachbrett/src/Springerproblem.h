/*
 * Springerproblem.h
 *
 *  Created on: 15.03.2019
 *  Author: sefi16
 */

#ifndef SPRINGERPROBLEM_H
#define SPRINGERPROBLEM_H

#include <list>
#include "Schachbrett.h"
#include "Coord.h"
#include "PossibleMoves.h"


class Springerproblem {

	private:
		Schachbrett m_schachbrett;
		std::list<Coord> m_pathList; // creates an empty list
		std::list<PossibleMoves> m_possibleMovesList; // creates an empty list

	public:
		Springerproblem();
		~Springerproblem();
		void InitializeChessboard(int n); // Create nxn chessboard
		void PrintChessboard();
		void PrintPathList();
		bool IsValidField(int iRow, int iCol);
		bool FindPathFrom(int iRow, int iCol); // Start finding path from field (iRow,iCol)

};


#endif /* SPRINGERPROBLEM_H */
