/*
 * Schachbrett.cpp
 *
 *  Created on: 13.03.2019
 *  Author: sefi16
 */
#include <iostream>
#include "Schachbrett.h"

// Standardkonstruktor
Schachbrett::Schachbrett() : m_iSize(0) {
	m_pBrett = {}; // NULL pointer on array of pointers
}

// Standarddesstruktor
Schachbrett::~Schachbrett() {
	for (int i = 0; i < m_iSize; i++) {
		delete[] m_pBrett[i];
	}
	delete[] m_pBrett;
}

void Schachbrett::Initialize(int iSize) {
	m_iSize = iSize;
	// Schachbrett als zwei-dimensionales Array
	m_pBrett = new int*[iSize];
	for (int i = 0; i < iSize; i++) {
		m_pBrett[i] = new int[iSize];
	}
	// Initialisiere alle Felder mit 0
	for (int row = 0; row < iSize; row++) {
		for (int col = 0; col < iSize; col++) {
			m_pBrett[row][col] = 0;
		}
	}
}

int Schachbrett::GetSize() {
	return m_iSize;
}

bool Schachbrett::IsValidField(int iRow, int iCol) {
	if (iRow < 0 || iCol < 0 || iRow >= m_iSize || iCol >= m_iSize)
		return false;
	return true;
}

bool Schachbrett::IsFreeField(int iRow, int iCol) {
	if (m_pBrett[iRow][iCol] == 0) return true;
	return false;
}

bool Schachbrett::SetField(int iRow, int iCol) {
	if (!IsValidField(iRow,iCol)) return false;
	m_pBrett[iRow][iCol] = 1;
	return true;
}

bool Schachbrett::UnsetField(int iRow, int iCol) {
	if (!IsValidField(iRow,iCol)) return false;
	m_pBrett[iRow][iCol] = 0;
	return true;
}

void Schachbrett::PrintBoard() {
	if (m_iSize <= 0) return;
	for (int i = 0; i < m_iSize; i++) {
		for (int j = 0; j < m_iSize; j++) {
			std::cout << " " << ((m_pBrett[i][j] == 0) ? 0 : 1);
		}
		std::cout << std::endl;
	}
}
