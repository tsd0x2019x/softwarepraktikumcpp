/*
 * Schachbrett.h
 *
 *  Created on: 13.03.2019
 *  Author: sefi16
 */

#ifndef SCHACHBRETT_H
#define SCHACHBRETT_H

class Schachbrett {

	private:
		int m_iSize; // Groesse des quadratischen Schachbretts.
		int ** m_pBrett; // Schachbrett als zwei-dimensionales Array

	public:
		Schachbrett();
		~Schachbrett();
		void Initialize(int iSize);
		int GetSize();
		bool IsValidField(int iRow, int iCol);
		bool IsFreeField(int iRow, int iCol); // Prüft ob das Feld schon besetzt ist
		bool SetField(int iRow, int iCol);
		bool UnsetField(int iRow, int iCol);
		void PrintBoard();
};


#endif /* SCHACHBRETT_H */
