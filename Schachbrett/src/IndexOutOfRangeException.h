/*
 * IndexOutOfRangeException.h
 *
 *  Created on: 17.03.2019
 *  Author: sefi16
 */

#ifndef INDEXOUTOFRANGEEXCEPTION_H
#define INDEXOUTOFRANGEEXCEPTION_H

#include <exception>

class IndexOutOfRangeException : public std::exception {

	public:
		virtual const char* what() const throw() {
			return "[IndexOutOfRangeException]: Index value is greater than equal size() or less than 0.";
		}
};



#endif /* INDEXOUTOFRANGEEXCEPTION_H */
