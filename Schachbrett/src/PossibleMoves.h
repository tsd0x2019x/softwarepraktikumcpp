/*
 * PossibleMoves.h
 *
 *  Created on: 17.03.2019
 *  Author: sefi16
 */

#ifndef POSSIBLEMOVES_H
#define POSSIBLEMOVES_H

#include <list>
#include "Coord.h"

class PossibleMoves {

	public:
		std::list<Coord> m_list; // Create an empty list
		std::list<Coord>::iterator iter;
		PossibleMoves();
//		~PossibleMoves();
		int GetNumberOfMoves();
		Coord GetMoveFromIndex(int index); // index 0..size()-1
		Coord PopLastMove(); // pop the last element from list

};

#endif /* POSSIBLEMOVES_H */
