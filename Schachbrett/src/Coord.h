/*
 * Coord.h
 *
 *  Created on: 17.03.2019
 *      Author: sefi16
 */

#ifndef COORD_H
#define COORD_H

struct Coord {
	int row, col;
	Coord() {
		row = -1;
		col = -1;
	}
	Coord(int iRow, int iCol) {
		row = iRow;
		col = iCol;	}

};


#endif /* FIELD_H */
