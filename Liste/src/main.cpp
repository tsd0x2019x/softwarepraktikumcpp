/*
 * main.cpp
 *
 *  Created on: 13.03.2019
 *  Author: sefi16
 */
#include <iostream>
#include <list>
#include "coord.h"


void delcoords(std::list<coord>& mList) {
	for(std::list<coord>::iterator mIte = mList.begin(); mIte != mList.end();) {
		if (mIte->x < mIte->y) {
			mIte = mList.erase(mIte);
		} else {
			mIte++;
		}
	}
}

void printlist(std::list<coord>* pList) {
	for(std::list<coord>::iterator mIte = pList->begin(); mIte != pList->end(); mIte++) {
		std::cout << "X: " << mIte->x << ", Y: " << mIte->y << std::endl;
	}
}


int main() {

	coord K1 = coord(1,1.2);
	coord K2 = coord(2.3,3.4);
	coord K3 = coord(1.4,2.5);
	coord K4 = coord(4.5,6.6);

	std::list<coord> mList, mBackupList;

	mList.push_back(K1);
	mList.push_back(K2);
	mList.push_back(K3);
	mList.push_back(K4);

	mBackupList.push_back(K1);
	mBackupList.push_back(K2);
	mBackupList.push_back(K3);
	mBackupList.push_back(K4);

	delcoords(mList);

	std::cout << "Backup List:" << std::endl;

	printlist(&mBackupList);

	std::cout << std::endl << "List:" << std::endl;

	printlist(&mList);

	mList.clear();
	mBackupList.clear();

	return 0;
}


