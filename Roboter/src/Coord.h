/*
 * Coord.h
 *
 *  Created on: 14.03.2019
 *  Author: sefi16
 */

#ifndef COORD_H
#define COORD_H

struct Coord {
	double dX;
	double dY;
	double dV;
	Coord(double dX, double dY, double dV) {
		this->dX = dX;
		this->dY = dY;
		this->dV = dV;
	}
};



#endif /* COORD_H */
