/*
 * RobotControl.h
 *
 *  Created on: 14.03.2019
 *  Author: sefi16
 */

#ifndef ROBOTCONTROL_H
#define ROBOTCONTROL_H

#include "InterfaceSIM.h"
#include "Maneuver.h"
#include "PosEstimation.h"
#include "PIDController.h"

class RobotControl {

	const double Kp = 500.0;
	const double Ki = 100.0;
	const double Kd = 0.0;
	const double timestep = 0.04;

	private:
		InterfaceSIM Interface;
		Maneuver m_Maneuver;
		PosEstimation posEstimation;
		PIDController motorR;
		PIDController motorL;
		bool bIsActive;
		int * iMicros; // ServoSignallänge für Motoren
		double adCurrentSpeed[2];
	public:
		RobotControl();
		~RobotControl();
		static RobotControl * transferPointer;
		static void transferFuntion();
		bool isActive();
		void Step();
		void Communicate();

};



#endif /* ROBOTCONTROL_H */
