/*
 * PosEstimation.h
 *
 *  Created on: 14.03.2019
 *  Author: sefi16
 */

#ifndef POSESTIMATION_H
#define POSESTIMATION_H

#include <cmath>

class PosEstimation {

	private:
		double * x; // double[3] mit x[0]: x-Koordiinate, x[1]: y-Koordinate, x[2]: Winkel
		double vAvarage; // Mittlere Geschwindigkeit

	public:
		PosEstimation();
		~PosEstimation();
		void Reset();
		void PredictPosition(double dSpeedR, double dSpeedL, double dTimestep);
		double * GetPosition();

};

#endif /* POSESTIMATION_H */
