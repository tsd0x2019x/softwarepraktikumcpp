/*
 * Maneuver.h
 *
 *  Created on: 14.03.2019
 *  Author: sefi16
 */

#ifndef MANEUVER_H
#define MANEUVER_H

#include <list>
#include <string>

class Maneuver{

	private:
		struct Coord {
			double x, y, v;
			Coord(double dX, double dY, double dV) {
				x = dX;
				y = dY;
				v = dV;
			}
		};
		std::list<Coord> CoordList;
		std::list<Coord>::iterator iter;
		bool bIsRunning;
		double adWishSpeed[2];
		double dMaxSpeed;
		double dPosDifference; // Maximale Abweichung

	public:
		Maneuver();
		~Maneuver();
		void CalcCircle(double dRadius, double dSpeed, double dTimestep);
		void CalcEight(double dRadius, double dSpeed, double dTimestep);
		void LogList(std::string sDatei);
		bool isRunning();
		void Start();
		void Stop();
		void Proceed();
		void CalcManeuverSpeed(double dX, double dY, double dW);
		double * GetManeuverSpeed();

};


#endif /* MANEUVER_H */
