/*
 * KeyboardControl.h
 *
 *  Created on: 13.03.2019
 *  Author: sefi16
 */

#ifndef KEYBOARDCONTROL_H
#define KEYBOARDCONTROL_H

#define SPEED_MAX 0.500000
#define SPEED_MIN -0.500000

#include "InterfaceSIM.h"
#include "PIDController.h"

#define M_KP 500.0
#define M_KI 1850.0
#define M_KD 0.0
#define M_TA 0.04

class KeyboardControl {

	private:
		InterfaceSIM m_ISIM;
		int * iMicros;
		/* Die Arrays speichern an der Stelle 0 die Geschwindigkeit des rechten und an der Stelle 1 die Geschwindigkeit des linken Motors */
		double * m_speedSoll; // Wunschgeschwindigkeit
		double * m_speedIst; // Aktuelle Geschwindigkeit
		PIDController * pidMotor0; // rechter Motor
		PIDController * pidMotor1; // linker Motor
		void StopMotors();

	public:
		KeyboardControl();
		~KeyboardControl();
		static KeyboardControl * transferPointer;
		static void transferFunction(void);
		void Communicate();
		void Step();

};


#endif /* KEYBOARDCONTROL_H */
