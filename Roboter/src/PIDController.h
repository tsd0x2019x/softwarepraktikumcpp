/*
 * PIDController.h
 *
 *  Created on: 14.03.2019
 *  Author: sefi16
 */

#ifndef PIDCONTROLLER_H
#define PIDCONTROLLER_H

class PIDController {

	private:
		double dKp, dKi, dKd; // Verstärkungsfaktoren
		double dTa; // Abtastzeit [s]
		double dEsum; // Summieren der Regelabweichungen über jeden Zeitschritt k zu E_k (= dEsum)
		double deold; // vorheriger Zeitschritt e_k (= deold)
		double dU; // Stellegröße u_k (dU) zum Zeitpunkt k

	public:
		PIDController(double Kp, double Ki, double Kd, double Ta);
		~PIDController();
		void CalculateU(double dW, double dY); // Berechnung der Stellgröße U = u_k
		double GetU();
		void SetFactors(double Kp, double Ki, double Kd);

};

#endif /* PIDCONTROLLER_H */
