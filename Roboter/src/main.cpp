/*
 * main.cpp
 *
 *  Created on: 13.03.2019
 *  Author: sefi16
 */
#include <iostream>
#include <string>
#include "KeyboardControl.h"
#include "PIDController.h"
#include "PosEstimation.h"
#include "Maneuver.h"
#include "RobotControl.h"

bool PosEstimationVergleich(std::string Dateiname, PosEstimation& PE) {
	std::ifstream iFile; // Filestream nur mit Lesezugriff
	iFile.open(Dateiname.c_str()); // Konvertiert Dateiname in C-String
	if (!iFile.is_open()) return false; // Datei kann nicht geöffnet werden
	double vrechts = 0;
	double vlinks = 0;
	double zeitschritt = 0;
	double * pos;
	while (!iFile.eof()) {
		iFile >> vrechts;
		iFile >> vlinks;
		iFile >> zeitschritt;
		// Berechung der Stellgröße dU
		PE.PredictPosition(vrechts, vlinks, zeitschritt);
		pos = PE.GetPosition();
		std::cout << pos[0] << " " << pos[1] << " " << pos[2] << std::endl;
	}
	delete[] pos;
	iFile.close();
	return true;
}

bool PIDControllerVergleich(std::string Dateiname, PIDController& PID) {
	std::ifstream iFile; // Filestream nur mit Lesezugriff
	iFile.open(Dateiname.c_str()); // Konvertiert Dateiname in C-String
	if (!iFile.is_open()) return false; // Datei kann nicht geöffnet werden
	double dW = 0; // Sollwert
	double dY = 0; // Istwert
	while (!iFile.eof()) {
		iFile >> dW; // Sollwert
		iFile >> dY; // Istwert
		// Berechung der Stellgröße dU
		PID.CalculateU(dW,dY);
		std::cout << dW << " " << dY << " => " << PID.GetU() << std::endl;
	}
	iFile.close();
	return true;
}


int main() {

//	KeyboardControl KC = KeyboardControl();
//	KC.Communicate();

//	double Kp = 1000, Ki = 10, Kd = 0.1, Ta = 0.5;
//	PIDController PID = PIDController(Kp,Ki,Kd,Ta);
//	std::string Dateiname = "/mnt/hgfs/Z/Roboter/PIDControllerInput.txt";
//	PIDControllerVergleich(Dateiname, PID);

//	PosEstimation PE = PosEstimation();
//	std::string Dateiname = "/mnt/hgfs/Z/Roboter/PosEstimationInput.txt";
//	PosEstimationVergleich(Dateiname, PE);

//	Maneuver MA = Maneuver();
//	MA.CalcCircle(2.0,0.3,0.04); // Kreis
//	std::string Dateiname = "/mnt/hgfs/Z/Roboter/LogFileCircle.txt";
//	MA.LogList(Dateiname);
//	MA.CalcEight(5.0,1.0,10.0); // Acht
//	Dateiname = "/mnt/hgfs/Z/Roboter/LogFileEight.txt";
//	MA.LogList(Dateiname);

	RobotControl RC = RobotControl();

	do {
		RC.Communicate();
	} while(RC.isActive());

	return 0;
}





