/*
 * square.h
 *
 *  Created on: 12.03.2019
 *  Author: sefi16
 */
#ifndef SQUARE_H
#define SQUARE_H

class square
{
	private:
		double m_laenge; // Kantenlänge

	public:
		square(double laenge);
		square(const square& Quadrat);
		~square();
		double getLaenge(); // Kantenlänge
		double getFlaeche();
		double getUmfang();

};

#endif // SQUARE_H
