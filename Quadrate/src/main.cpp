/*
 * main.cpp
 *
 *  Created on: 12.03.2019
 *  Author: sefi16
 */
#include <iostream>
#include <string>
#include "square.h"

// Überladung des Operators + (Overloading of operator +)
square operator+(square Quadrat1, square Quadrat2) {
	return square(Quadrat1.getLaenge() + Quadrat2.getLaenge());
}

// Überladung des Operators - (Overloading of operator -)
square operator-(square Quadrat1, square Quadrat2) {
	return (Quadrat1.getLaenge() > Quadrat2.getLaenge()) ? square(Quadrat1.getLaenge() - Quadrat2.getLaenge()) : square(Quadrat2.getLaenge() - Quadrat1.getLaenge());
}

// Überladung des Operators << (Overloading of operator <<)
std::ostream& operator<<(std::ostream& os, square& Quadrat) {
	return os << "Quadrat: Kantenlänge=" << Quadrat.getLaenge() << ", Fläche=" << Quadrat.getFlaeche() << ", Umfang=" << Quadrat.getUmfang();
}

int main() {

	square Q_1 = square(10);
	square Q_2 = square(Q_1);

	std::cout << "Q_1: " << Q_1 << std::endl;
	std::cout << "Q_2: " << Q_2 << std::endl;

	square Q_Sum = Q_1 + Q_2;
	std::cout << "Q_1 + Q_2: " << Q_Sum << std::endl;

	std::cout << std::endl;

	square Q_3 = square(9);
	std::cout << "Q_3: " << Q_3 << std::endl;

	std::cout << std::endl;

	square Q_Dif = Q_1 - Q_3;
	std::cout << "Q_1 - Q_3: " << Q_Dif << std::endl;

	return 0;
}




