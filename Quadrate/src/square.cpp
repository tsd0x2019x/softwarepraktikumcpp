/*
 * square.cpp
 *
 *  Created on: 12.03.2019
 *  Author: sefi16
 */
#include "square.h"

square::square(double laenge) {
	m_laenge = laenge;
}

square::square(const square& Quadrat) {
	m_laenge = Quadrat.m_laenge;
}

// Standarddestruktor
square::~square() {}

 double square::getLaenge() {
	return m_laenge;
}

double square::getFlaeche() {
	return m_laenge * m_laenge;
}

double square::getUmfang() {
	return 4 * m_laenge;
}



