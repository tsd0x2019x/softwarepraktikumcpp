//============================================================================
// Name        : QuadratischeFunktion.cpp
// Author      : sefi16
// Version     :
// Copyright   : Your copyright notice
// Description : Quadratische Funktionen in C++, Ansi-style
//============================================================================

/**
 * Teste ob die quadratische Funktion a*x² + b*x + c = 0 eine reelle Lösung hat.
 * Wenn ja, berechne diese reellen Lösungen.
 */
#include <iostream>
using namespace std;

/**
 * Diese Methode berechnet iterativ (nach dem Heron-Verfahren) die Quadratwurzel einer Zahl.
 */
float heron_sqrt(/* Rootzahl */ int a, /* Startwert, default=10 */ float x0) {
	if ( a == 0 ) return 0;

	int ite_num = 10; // Number of iterations
	float xn = x0;

	while ( ite_num > 0 ) {
		xn = xn + (a / xn);
		xn /= 2;
		ite_num -= 1;
	}

	return xn;
}

/**
 * Prüft, ob die gegebene quadratische Funktion lösbar ist, d.h. eine (reelle) Lösung hat.
 * @return
 * 		0 	Keine Lösung (nicht lösbar)
 * 		1	Es gibt genau eine Lösung
 * 		2 	Es gibt genau zwei Lösungen
 */
int checkIfSolvable (float a, float b, float c, float * x1, float * x2) {
	/* Bestimme die Diskriminate */
	float D = b*b-4*a*c;

	if ( D < 0 ) { // Es gibt keine (reelle) Lösung
		return 0;
	}
	else if ( D == 0 ) { // Es gibt genau eine Lösung
		*x1 = (-b + heron_sqrt(D,10))/(2*a);
		return 1;
	}
	else { // D > 0. Es gibt zwei Lösungen
		*x1 = (-b + heron_sqrt(D,10))/(2*a);
		*x2 = (-b - heron_sqrt(D,10))/(2*a);
		return 2;
	}
}

int main() {
	cout << "*** Quadratische Funktionen: ax² + bx + c = 0 ***" << endl;

	float a = 0, b = 0, c = 0; // Koeffizienten
	float x1 = 0, x2 = 0; // Lösungen
	int iNumberSolutions = -1;

	cout << "Bitte Koeffizienten a, b, c eingeben." << endl;

	cout << "a = ";
	cin >> a;

	cout << "b = ";
	cin >> b;

	cout << "c = ";
	cin >> c;

	// Prüfe, ob die quadratische Funktion lösbar ist.
	iNumberSolutions = checkIfSolvable(a,b,c,&x1,&x2);

	// Ausgabe des Ergebnisses
	cout << endl;
	cout << "Funktion: " << a << "x^2 + " << b << "x + " << c << " = 0" << endl;
	cout << endl;

	if ( iNumberSolutions == 0 ) {
		cout << "Keine Lösung" << endl;
	}
	else if ( iNumberSolutions == 1 ) {
		cout << "Eine Lösung: x = " << x1 << endl;
	}
	else {
		cout << "Zwei Lösungen: x1 = " << x1 << ", x2 = " << x2 << endl;
	}

	return 0;
}
