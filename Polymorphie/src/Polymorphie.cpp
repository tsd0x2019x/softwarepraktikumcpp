/*
 * Polymorphie.cpp
 *
 *  Created on: 12.03.2019
 *  Author: sefi16
 */

#include "Polymorphie.h"

int main() {

	A a = A(); a.f();
	B b = B(); b.g();
	C c = C(); c.g();

	//B b0 = A(); b0.g();  // <-- Ein B ist A, aber A muss nicht zwingend B sein => Error
	A a1 = B(); a1.f();
	A a2 = b; a2.f();
	A * a3 = &b; a3->f();

	return 0;
}




