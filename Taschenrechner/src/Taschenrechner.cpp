//============================================================================
// Name        : Taschenrechner.cpp
// Author      : sefi16
// Version     :
// Copyright   : Your copyright notice
// Description : Taschenrechner in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
using namespace std;

int main() {
	cout << "*** Aufgabe: Taschenrechner ***" << endl;

	const int NUM_IGNORE_CHARS = 200;
	float fNumber = 0, fRes = 0; // Einzugebende Zahl und Ergebnis
	char cOperator = '+'; // Operator (+,-,*,/)

	/*
	 * a) Eingabe der ersten Zahl
	 */
	cout << "Erste Zahl:" << endl;
	cin >> fNumber;

	while (cin.fail()) {
		cout << "ERROR::Falscheingabe: Bitte eine Zahl eingeben" << endl << "Erste Zahl:" << endl;
		cin.clear();
		cin.ignore(NUM_IGNORE_CHARS, '\n');
		cin >> fNumber;
	}

	/*
	 * b) Eingabe des Operators
	 */
	cout << "Operator (+,-,*,/):" << endl;
	cin >> cOperator;

	while (cin.fail() || !(cOperator == '+' || cOperator == '-' || cOperator == '*' || cOperator == '/')) {
		cout << "ERROR::Falscheingabe: Bitte ein Operator (+,-,*,/) eingeben" << endl << "Operator (+,-,*,/):" << endl;
		cin.clear();
		cin.ignore(NUM_IGNORE_CHARS, '\n');
		cin >> cOperator;
	}

	fRes = fNumber;

	/* Zweite Zahl */
	cout << "Zweite Zahl:" << endl;
	cin >> fNumber;

	while (cin.fail()) {
		cout << "ERROR::Falscheingabe: Bitte eine Zahl eingeben" << endl << "Zweite Zahl:" << endl;
		cin.clear();
		cin.ignore(NUM_IGNORE_CHARS, '\n');
		cin >> fNumber;
	}

	if ( cOperator == '/' && fNumber == 0 ) {
		cout << "Ergebnis: Division durch 0 nicht zulässig!" << endl;
		return 0;
	}

	/* Berechnung ausführen */
	switch (cOperator) {
		case '+':
			fRes += fNumber;
			break;
		case '-':
			fRes -= fNumber;
			break;
		case '*':
			fRes *= fNumber;
			break;
		case '/':
			fRes /= fNumber;
			break;
		default:
			break;
	}

	/* Ausgabe des Ergebnisses */
	cout << "Ergebnis: " << fRes << endl;

	return 0;
}
