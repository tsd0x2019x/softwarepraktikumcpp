/*
 * Vehicle.cpp
 *
 *  Created on: 12.03.2019
 *  Author: sefi16
 */
#include <iostream>
#include <string>
#include "Vehicle.h"

int Vehicle::siCountID = 1; // Initialisierung der statischen Membervariable

// Standardkonstruktor
Vehicle::Vehicle(t_farbe farbe, float preis, int baujahr) {
	m_farbe = farbe;
	m_preis = preis;
	m_baujahr = baujahr;

	m_ID = siCountID;
	siCountID++;
}

// Standarddestruktor
Vehicle::~Vehicle() {}

// Statische Methode
int Vehicle::getCount() {
	return Vehicle::siCountID;
}

// Statische Methode
bool Vehicle::isOldtimer(Vehicle fahrzeug) {
	return (fahrzeug.m_baujahr < 1980) ? true :false;
}

std::string Vehicle::getFarbe() {
	switch (m_farbe) {
		case blau:
			return "blau";
			break;
		case rot:
			return "rot";
			break;
		case gruen:
			return "grün";
			break;
		case weiss:
			return "weiss";
			break;
		default: // schwarz
			return "schwarz";
			break;
	}
}

float Vehicle::getPreis() {
	return m_preis;
}

int Vehicle::getBaujahr() {
	return m_baujahr;
}

void Vehicle::printProperties() {
	std::cout << "Farzeug " << m_ID << " => Farbe: " << getFarbe() << ", Preis: " << m_preis << ", Baujahr: " << m_baujahr << std::endl;
}




