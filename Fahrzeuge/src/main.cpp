/*
 * main.cpp
 *
 *  Created on: 12.03.2019
 *  Author: sefi16
 */
#include <iostream>
#include <string>
#include "Vehicle.h"

int main() {

	Vehicle fahrzeug1 = Vehicle(Vehicle::blau, 20000, 2016);
	Vehicle fahrzeug2 = Vehicle(Vehicle::rot, 1000, 1974);
	Vehicle fahrzeug3 = Vehicle(Vehicle::gruen, 4000, 1969);
	Vehicle fahrzeug4 = Vehicle(Vehicle::weiss, 68000, 2019);

	fahrzeug1.printProperties();
	fahrzeug2.printProperties();
	fahrzeug3.printProperties();
	fahrzeug4.printProperties();

	if (Vehicle::isOldtimer(fahrzeug1)) std::cout << "Farhzeug 1 ist ein OLDTIMER." << std::endl;
	if (Vehicle::isOldtimer(fahrzeug2)) std::cout << "Farhzeug 2 ist ein OLDTIMER." << std::endl;
	if (Vehicle::isOldtimer(fahrzeug3)) std::cout << "Farhzeug 3 ist ein OLDTIMER." << std::endl;
	if (Vehicle::isOldtimer(fahrzeug4)) std::cout << "Farhzeug 4 ist ein OLDTIMER." << std::endl;

	return 0;
}




