/*
 * Vehicle.h
 *
 *  Created on: 12.03.2019
 *  Author: sefi16
 */
#ifndef VEHICLE_H
#define VEHICLE_H

#include <string>

class Vehicle
{
	public:
		enum t_farbe {blau,rot,gruen,weiss,schwarz};
		Vehicle(t_farbe farbe, float preis, int baujahr);
		~Vehicle();
		int m_ID;
		static int getCount();
		static bool isOldtimer(Vehicle fahrzeug);
		std::string getFarbe();
		float getPreis();
		int getBaujahr();
		void printProperties();

	private:
		t_farbe m_farbe;
		float m_preis;
		int m_baujahr;
		static int siCountID;

};

#endif /* VEHICLE_H */

