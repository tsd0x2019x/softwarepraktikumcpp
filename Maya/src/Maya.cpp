//============================================================================
// Name        : Maya.cpp
// Author      : sefi16
// Version     :
// Copyright   : Your copyright notice
// Description : Maya Zahlen in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int potenz(int m, unsigned int i) {
	if ( i == 0 ) return 1;
	int res = 1;
	while ( i-- > 0 ) {
		res *= m;
	}
	return res;
}

int main() {

	const int NUM_IGNORE_CHARS = 200;
	unsigned int /* Dezimalzahl */ iNumber = 0, /* Ergebnis */ iResult = 0, /* Zaehler */ i = 0;

	cout << "*** Maya Zahlen ***" << endl;

	cout << "Bitte eine Dezimalzahl eingeben" << endl;
	cin >> iNumber;

	while (cin.fail()) {
		cout << "ERROR::Falscheingabe: Bitte eine Dezimalzahl eingeben" << endl;
		cin.clear();
		cin.ignore(NUM_IGNORE_CHARS, '\n');
		cin >> iNumber;
	}

	/* Umrechnung von Dezimalsystem in Maya-System (Zwanzigersystem) */
	while (iNumber > 0) {
		iResult += (iNumber % 20) * potenz(10,i++);
		iNumber /= 20;
	}

	/* Ausgabe des Ergebnisses */
	cout << "Ergebnis: " << iResult << endl;;

	return 0;
}
