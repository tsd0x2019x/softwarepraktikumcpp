//============================================================================
// Name        : Referenzen.cpp
// Author      : sefi16
// Version     :
// Copyright   : Your copyright notice
// Description : Referenzen in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

void swap(int& a, int& b) {
	int iTemp = a;
	a = b;
	b = iTemp;
}

int main() {
	cout << "*** Referenzen ***" << endl;

	int a = 0, b = 0;

	cout << "Bitte zwei Parameter übergeben." << endl;

	cout << "Paramter 1: ";
	cin >> a;

	cout << "Paramter 2: ";
	cin >> b;

	cout << "Vertauschen von Parametern." << endl;
	swap(a,b);

	cout << "Paramter 1: " << a << endl;
	cout << "Paramter 2: " << b << endl;

	return 0;
}
