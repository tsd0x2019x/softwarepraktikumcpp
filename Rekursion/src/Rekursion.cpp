/*
 * Rekursion.cpp
 *
 *  Created on: 13.03.2019
 *  Author: sefi16
 */
#include "Rekursion.h"

// Standardkonstruktor
Rekursion::Rekursion() {}

// Standarddesstruktor
Rekursion::~Rekursion() {}

/**
 * Berechnung der Fakultät einer nicht-negativen ganzen Zahl.
 * Iterative Implementierung.
 */
unsigned long Rekursion::factorialIter(int iNumber) {
	if (iNumber <= 1) return 1;
	else {
		int iFac = 1;
		while (iNumber > 1) {
			iFac *= iNumber;
			--iNumber;
		}
		return iFac;
	}
}

/**
 * Berechnung der Fakultät einer nicht-negativen ganzen Zahl.
 * Rekursive Implementierung.
 */
unsigned long Rekursion::factorialRec(int iNumber) {
	return (iNumber <= 1) ? 1 : iNumber * factorialRec(iNumber-1);
}

/**
 * Berechnung der Fibonacci-Folge
 */
unsigned long Rekursion::fibonacci(int iNumber) {
	return (iNumber <= 0) ? 0 : (iNumber < 2) ? 1 : fibonacci(iNumber-1) + fibonacci(iNumber-2);

}



