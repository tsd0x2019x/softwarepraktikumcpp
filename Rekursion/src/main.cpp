/*
 * main.cpp
 *
 *  Created on: 13.03.2019
 *  Author: sefi16
 */
#include <iostream>
#include "Rekursion.h"

/*
 * Gibt die ersten <iLimit> Glieder der Fibonacci-Folge aus
 */
void printSequenceFibonacci(Rekursion R, int iLimit) {
	std::cout << "[Fibonacci] ";
	for (int i = 1; i <= iLimit; i++)
		std::cout << R.fibonacci(i) << ", ";
	std::cout << std::endl;
}

/*
 *
 */
void printFibonacci(Rekursion R) {
	int iNumber = 0;

	std::cout << "[Fibonacci] Bitte eine nicht-negative Zahl eingeben: ";
	std::cin >> iNumber;

	while (std::cin.fail()) {
		std::cout << "[Fibonacci] Bitte eine nicht-negative Zahl eingeben: ";
		std::cin.clear();
		std::cin.ignore(200, '\n');
		std::cin >> iNumber;
	}

	std::cout << "[Fibonacci] f(" << iNumber << ") = " << R.fibonacci(iNumber) << std::endl;
}

/*
 *
 */
void printFakultaet(Rekursion R) {
	int iNumber = 0;

	std::cout << "[Fakultät] Bitte eine nicht-negative Zahl eingeben: ";
	std::cin >> iNumber;

	while (std::cin.fail()) {
		std::cout << "[Fakultät] Bitte eine nicht-negative Zahl eingeben: ";
		std::cin.clear();
		std::cin.ignore(200, '\n');
		std::cin >> iNumber;
	}

	std::cout << "[Fakultät] Iterativ: " << iNumber << "! = " << R.factorialIter(iNumber) << std::endl;
	std::cout << "[Fakultät] Rekursiv: " << iNumber << "! = " << R.factorialRec(iNumber) << std::endl;
}

int main() {

	Rekursion R = Rekursion();

	// Berechnung der Fakultät
	//printFakultaet(R);

	// Berechnung der Fibonacci-Folge
	//printFibonacci(R);

	// Gibt die ersten 15 Glieder der Fibonacci-Folge aus
	printSequenceFibonacci(R, 15);

	return 0;
}




