/*
 * Rekursion.cpp
 *
 *  Created on: 13.03.2019
 *  Author: sefi16
 */

#ifndef REKURSION_H
#define REKURSION_H

class Rekursion {

	private:

	public:
		Rekursion();
		~Rekursion();
		unsigned long factorialIter(int iNumber); // Iterative Implementierung der Fakultät
		unsigned long factorialRec(int iNumber); // Rekursive Implementierung der Fakultät
		unsigned long fibonacci(int iNumber); // Fibonacci-Folge
};

#endif /* REKURSION_H */




