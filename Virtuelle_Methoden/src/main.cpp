/*
 * main.cpp
 *
 *  Created on: 12.03.2019
 *  Author: sefi16
 */
#include <iostream>
#include "Student.h"

int main() {

	int iNumber = 4;

	/*
	 * a)
	 */
	Person * pPerson[iNumber];

	/*
	 * b)
	 */
	for(int i = 0; i < iNumber; i++) {
		pPerson[i] = new Student;
		std::cout << "Person " << i << ": " << pPerson[i]->getName() << ", " << pPerson[i]->getSurname() << std::endl;
	}



	/*
	 * c)
	 */


	return 0;
}




