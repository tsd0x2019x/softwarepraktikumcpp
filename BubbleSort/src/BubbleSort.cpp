//============================================================================
// Name        : BubbleSort.cpp
// Author      : sefi16
// Version     :
// Copyright   : Your copyright notice
// Description : BubbleSort in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

void printArray(int A[], int iSize) {
	for (int i = 0; i < iSize; i++) {
		cout << A[i] << " ";
	}
	cout << endl;
}

void swap(int& a, int& b) {
	int tmp = a;
	a = b;
	b = tmp;
}

void BubbleSort(int A[], int iSize) {
	for (int n = iSize; n > 1; n--) {
		for (int i = 0; i < n-1; i++) {
			if (A[i] > A[i+1]) swap(A[i], A[i+1]);
		}
	}
}

int main() {
	cout << "*** BubbleSort ***" << endl;

	int iSize = 32;
	int * iArray = new int[iSize];

	srand(time(NULL));

	for (int i = 0; i < iSize; i++) {
		iArray[i] = rand() % 101; // Zufallszahlen zwischen 0 und 100
	}

	printArray(iArray, iSize);

	BubbleSort(iArray, iSize); // Komplexität ist O(n*n)

	printArray(iArray, iSize);

	delete[] iArray;

	return 0;
}
