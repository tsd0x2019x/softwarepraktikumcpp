/*
 * KeyboardControl.cpp
 *
 *  Created on: 13.03.2019
 *  Author: sefi16
 */
#include <ncurses/curses.h>
#include "KeyboardControl.h"

KeyboardControl::KeyboardControl() : pidMotor0(new PIDController(M_KP,M_KI,M_KD,M_TA)), pidMotor1(new PIDController(M_KP,M_KI,M_KD,M_TA)) {
	iMicros = new int[2];
	iMicros[0] = iMicros[1] = 1500;

	// Initialisiere Geschwindigkeiten auf Null
	m_speedIst = new double[2];
	m_speedSoll = new double[2];
	m_speedIst[0] = m_speedIst[1] = m_speedSoll[0] = m_speedSoll[1] = 0;

	// Erstelle und initialisiere Instanz von InterfaceSIM
	//m_ISIM = InterfaceSIM();
	//m_ISIM.Initialize(/* Zeitschrittlänge in [s] */ M_TA, transferFunction);
	m_IHW.Initialize(/* Zeitschritt [s] */ M_TA);
	m_IHW.SetTimerfunction(iTimer, M_TA, &transferFunction);

	// static transferPointer zeigt auf die Klasse selbst
	transferPointer = this; // muss am Ende des Kontruktors liegen
}

KeyboardControl::~KeyboardControl() {
	delete[] m_speedIst;
	delete[] m_speedSoll;
	delete pidMotor0;
	delete pidMotor1;
	delete[] iMicros;
}

KeyboardControl * KeyboardControl::transferPointer; // static

void KeyboardControl::transferFunction(void) { // static
	transferPointer->Step(); //  (*transferPointer).Step() == KeyboardControl().Step()
}

/*
 * Setzt Soll-Geschwindigkeit beider Motoren auf Null
 */
void KeyboardControl::StopMotors() {
	m_speedSoll[0] = m_speedSoll[1] = 0;
}

/*
 * Communicate() kommuniziert mit dem Benutzer bzw. mit der Tastatur, liest einzelne Zeichen des Benutzers ein
 * und stellt daraufhin die Wunschgeschwindigkeit (Soll-Geschwindigkeit) des Roboters ein.
 */
void KeyboardControl::Communicate() {

	// Starte die parallele Ausführung. Dadurch wird die Step()-Funktion zyklisch aufgerufen.
	//sigprocmask(SIG_UNBLOCK, &m_ISIM.mask, nullptr); // Step()-Methode starten

	char c = '0'; // Speichert Benutzereingabe

	// ncurses starten und initialisierung
	initscr();
	nodelay(stdscr, TRUE);
	noecho();

	do {
		c = getch(); // Benutzereingabe zeichenweise einlesen
		if (c != -1) { // Wenn etwas eingegeben wird.
			clear(); // Bildschirm löschen
			switch (c) {
				case 'a': // Die aktuelle Wunschgeschwindigkeit des rechten Motors wird um 0.005 erhöht und die des linken Motors um 0.005 vermidert.
					m_speedSoll[0] += 0.005;// rechter Motor
					m_speedSoll[1] -= 0.005;// linker Motor
					break;
				case 'b': // Anhalten
					StopMotors();
					break;
				case 'd': // Die aktuelle Wunschgeschwindigkeit des rechten Motors wird um 0.005 verringert und die des linken Motors um 0.005 erhöht.
					m_speedSoll[0] -= 0.005;// rechter Motor
					m_speedSoll[1] += 0.005;// linker Motor
					break;
				case 'q': // Anhalten und Verlassen der do-while-Schleife
					StopMotors();
					pidMotor0->SetFactors(0,0,0);
					pidMotor1->SetFactors(0,0,0);
					break;
				case 's': // rückwärts
					m_speedSoll[0] -= 0.01;	// rechter Motor
					m_speedSoll[1] -= 0.01; // linker Motor
					break;
				case 'w': // vorwärts
					m_speedSoll[0] += 0.01;	// rechter Motor
					m_speedSoll[1] += 0.01; // linker Motor
					break;
				default:
					break;
			}

			if (m_speedSoll[0] > SPEED_MAX) m_speedSoll[0] = SPEED_MAX;
			if (m_speedSoll[1] > SPEED_MAX) m_speedSoll[1] = SPEED_MAX;
			if (m_speedSoll[0] < SPEED_MIN) m_speedSoll[0] = SPEED_MIN;
			if (m_speedSoll[1] < SPEED_MIN) m_speedSoll[1] = SPEED_MIN;

			printw("Taste: %c, Sollgeschwindigkeit: Left(%f) Right(%f)", c, m_speedSoll[1], m_speedSoll[0]);
		}
	} while (c != 'q');

	while (m_speedIst[0] != 0 || m_speedIst[1] != 0) {
		StopMotors();
	}

	endwin(); // ncurses beenden

	// Beende die parallele Ausführung. Dadurch wird das zyklische Aufrufen der Step()-Funktion beendet.
	//sigprocmask(SIG_BLOCK, &m_ISIM.mask, nullptr);

}

/*
 * Organisiert die Kommunikation mit dem Roboter, nimmt den Wert der Soll-Geschwindigkeit, der über die Methode
 * Communicate() eingestellt wurde, und verarbeitet diesen. Dazu wird der Messwert der Geschwindigkeit
 * (Ist-Geschwindigkeit) eingelesen, ausgewertet und anschliessend gegebenfalls eine neue Stellgröße an den
 * Roboter gesendet.
 */
void KeyboardControl::Step() {
	// Ist-Geschwindigkeiten einlesen und zwischenspeichern. GetInput() darf nur einmal zu Beginn der Step-Methode aufgerufen werden.
	//double * _pdInput = m_ISIM.GetInput();
	double * _pdInput = m_IHW.GetInput();
	m_speedIst[0] = _pdInput[0]; // rechtes Motor
	m_speedIst[1] = _pdInput[1]; // linkes Motor

	// Sollgeschwindigkeiten von Communicate() verwenden und in Signallängen [1000,2000] [microsec] umrechnen
	// Formel: Signallaenge = Sollgeschwindigkeit * 1000 + 1500
	// wobei [-0.5,0.5] entspricht [1000,2000] und 0 entspricht 1500
	//int* iMicros = new int[2];

	//	iMicros[0] = (int) (m_speedSoll[0] * 1000 + 1500); // rechter Motor
	//	iMicros[1] = (int) (m_speedSoll[1] * 1000 + 1500); // linker Motor

	// Berechne die Stellgröße des jeweiligen Motors
	pidMotor0->CalculateU(m_speedSoll[0], m_speedIst[0]); // rechter Motor
	pidMotor1->CalculateU(m_speedSoll[1], m_speedIst[1]); // rechter Motor

	double U0 = pidMotor0->GetU();
	double U1 = pidMotor1->GetU();

	// Die berechneten Stellgrößen U werden noch in den Nullwert des Servosignals verschoben,
	// d.h. Auf die berechneten U müssen jeweils 1500 addiert werden => U := U + 1500.
	// Anschließend werden die neuen Stellgrößen U in Array iMicros kopiert.
	U0 += 1500;
	U1 += 1500;

	if (U0 > 2000) U0 = 2000;
	if (U0 < 1000) U0 = 1000;
	if (U1 > 2000) U1 = 2000;
	if (U1 < 1000) U1 = 1000;

	iMicros[0] = (int) U0;
	iMicros[1] = (int) U1;

	// Signallängen an Roboter senden => Aktualisiere die Geschwindigkeiten der Motoren
	m_IHW.SetOutputs(iMicros);

	//delete[] iMicros;
}


