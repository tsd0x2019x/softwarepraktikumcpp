/*
 * RobotControl.cpp
 *
 *  Created on: 15.03.2019
 *  Author: sefi16
 */
#include <iostream>
//#include "ncurses.h"
#include <ncurses/curses.h>
#include "RobotControl.h"

RobotControl::RobotControl() : motorR(Kp,Ki,Kd,timestep), motorL(Kp,Ki,Kd,timestep), bIsActive(true) {
	iMicros = new int[2]; // ServoSignallänge für Motoren
	iMicros[0] = 1500; // Nulllage, rechter Motor
	iMicros[1] = 1500; // Nulllage, linker Motor

	//Interface.Initialize(timestep, transferFuntion);
	Interface.Initialize(/* Zeitschritt [s] */ timestep);
	Interface.SetTimerfunction(iTimer, timestep, &transferFunction);

	// static transferPointer zeigt auf die Klasse selbst
	transferPointer = this; // muss am Ende des Kontruktors liegen
}

RobotControl::~RobotControl() {
	delete[] iMicros;
}

RobotControl * RobotControl::transferPointer; // static

void RobotControl::transferFunction(void) {
	transferPointer->Step(); //  (*transferPointer).Step() == RobotControl().Step()
}

bool RobotControl::isActive() {
	return bIsActive;
}

void RobotControl::Communicate() {
	char cEingabe = '0';
	std::cout << "Neues Manöver fahren? (J/N): ";
	std::cin >> cEingabe;
	while(std::cin.fail()) {
		std::cout << "Neues Manöver fahren? (J/N): ";
		std::cin.clear();
		std::cin.ignore(400, '\n');
		std::cin >> cEingabe;
	}
	if (cEingabe == 'n' || cEingabe == 'N') {
		bIsActive = false;
		return;
	}

	// Neues Manöver soll gefahren werden
	std::cout << "Ein neues Manöver wird gewählt und gerechnet." << std::endl;
	std::cout << "Bitte Wunschgeschwindigkeit und -radius eingeben." << std::endl;
	// Radius und Wunschgeschwindigkeit einlesen
	double dRadius = 0.0;
	double dSpeed = 0.0;
	std::cout << "Radius [m]: ";
	std::cin >> dRadius;
	while(std::cin.fail()) {
		std::cout << "Radius [m]: ";
		std::cin.clear();
		std::cin.ignore(400, '\n');
		std::cin >> dRadius;
	}
	std::cout << "Geschwindkeit [m/s]: ";
	std::cin >> dSpeed;
	while(std::cin.fail()) {
		std::cout << "Geschwindigkeit [m/s]: ";
		std::cin.clear();
		std::cin.ignore(400, '\n');
		std::cin >> dSpeed;
	}

	// Wunschmanöver einlesen
	int iManeuver = 0;
	std::cout << std::endl << "Bitte ein Manöver auswählen. Drücken Sie bitte 1 oder 2." << std::endl;
	std::cout << "   (1) Kreis          (2) Acht" << std::endl;
	std::cout << "Ihre Auswahl: ";
	std::cin >> iManeuver;
	while(std::cin.fail() || iManeuver < 1 || iManeuver > 2) {
		std::cout << std::endl << "Bitte ein Manöver auswählen. Drücken Sie bitte 1 oder 2." << std::endl;
		std::cout << "   (1) Kreis          (2) Acht" << std::endl;
		std::cout << "Ihre Auswahl: ";
		std::cin.clear();
		std::cin.ignore(500, '\n');
		std::cin >> iManeuver;
	}

	// Manöver erstellen/berechnen. Gestartet wird erst später.
	if (iManeuver == 1) { // Kreis
		m_Maneuver.CalcCircle(dRadius, dSpeed, timestep);
	}
	else { // Acht, iManeuver == 2
		m_Maneuver.CalcEight(dRadius, dSpeed, timestep);
	}

	// Positionsschätzung zurücksetzen
	posEstimation.Reset();

	// ncurses starten und initialisierung
	initscr();
	nodelay(stdscr, TRUE);
	noecho();

	// Starte die parallele Ausführung. Dadurch wird die Step()-Funktion zyklisch aufgerufen.
	//sigprocmask(SIG_UNBLOCK, &Interface.mask, nullptr); // Step()-Methode starten

	std::string sManeuver = (iManeuver == 1) ? "Kreis" : "Acht";
//	printw("\nManöver %s ist startbereit. Sie haben folgende Optionen:", sManeuver.c_str());
//	printw("\n  (1) Start    (2) Stop    (3) Proceed    (q) Verlassen\nIhre Auswahl: ");

	do {
		cEingabe = getch(); // Benutzereingabe zeichenweise einlesen
		if (cEingabe != -1) { // Wenn etwas eingegeben wird.
			clear(); // Bildschirm löschen
			switch (cEingabe) {
				case '1': // Start
					printw("\n  (1) Start    (2) Stop    (3) Proceed    (q) Verlassen\n\n");
					//printw("\nManöver %s gestartet...", sManeuver.c_str());
					m_Maneuver.Start();
					break;
				case '2': // Stop
					printw("\n  (1) Start    (2) Stop    (3) Proceed    (q) Verlassen\n\n");
//					printw("\nManöver %s gestoppt...", sManeuver.c_str());
					m_Maneuver.Stop();
					break;
				case '3': // Proceed
					printw("\n  (1) Start    (2) Stop    (3) Proceed    (q) Verlassen\n\n");
//					printw("\nManöver %s macht gerade...", sManeuver.c_str());
					m_Maneuver.Proceed();
					break;
				case 'q':
					printw("Manöver wird jetzt ausgeschaltet.");
//					motorR.SetFactors(0,0,0);
//					motorL.SetFactors(0,0,0);
					break;
				default:
					break;
			}
		}
	} while (cEingabe != 'q');

	// 'q' wurde gedrückt. Manöver wird gestoppt.
	m_Maneuver.Stop();

	// TODO ??????
	// Es wird noch in der Schleife überprüft, ob der Roboter wirklich angehalten hat. Es wird gewartet solange Roboter noch nicht anhält.
	while (adCurrentSpeed[0] != 0.0 || adCurrentSpeed[1]!= 0.0) {}

	// Roboter steht => Beende die parallele Ausführung. Dadurch wird das zyklische Aufrufen der Step()-Funktion beendet.
	//sigprocmask(SIG_BLOCK, &Interface.mask, nullptr); // Step()-Methode abschalten

	endwin(); // ncurses beenden

	std::cout << "\nManöver " << sManeuver << " wurde erfolgreich ausgeschaltet. Programm beendet!" << std::endl;
}

void RobotControl::Step() {
	// Aktuelle (Ist-) Geschwindigkeit des Roboters einlesen und zwischenspeichern
	double * p_adCurrentSpeed = Interface.GetInput();
	adCurrentSpeed[0] =  p_adCurrentSpeed[0];
	adCurrentSpeed[1] =  p_adCurrentSpeed[1];

	// Check ob Manöversteuerung noch aktiv
	if (!m_Maneuver.isRunning()) { // Nicht aktiv => Roboter stoppen
		iMicros[0] = 1500; // Ruhelage, rechter Motor
		iMicros[1] = 1500; // Ruhelage, linker Motor
	}
	else {
		// Positionsschätzung durchführen
		posEstimation.PredictPosition(/* rechter Motor */ adCurrentSpeed[0], /* linker Motor */ adCurrentSpeed[1], timestep);
		// Position und Winkel speichern
		double * adPos = posEstimation.GetPosition();
		// Neue Geschwindigkeiten berechnen und speichern
		m_Maneuver.CalcManeuverSpeed(adPos[0], adPos[1], adPos[2]);
		// Geschwindigkeiten mit Reglern in Signallängen umrechnen
		motorR.CalculateU(m_Maneuver.GetManeuverSpeed()[0], adCurrentSpeed[0]); // rechter Motor
		motorL.CalculateU(m_Maneuver.GetManeuverSpeed()[1], adCurrentSpeed[1]); // linker Motor

		double U0 = motorR.GetU();
		double U1 = motorL.GetU();

		// Die berechneten Stellgrößen U werden noch in den Nullwert des Servosignals verschoben,
		// d.h. Auf die berechneten U müssen jeweils 1500 addiert werden => U := U + 1500.
		// Anschließend werden die neuen Stellgrößen U in Array iMicros kopiert.
		U0 += 1500;
		U1 += 1500;

		// Zulässigkeit der SIgnallängen sicherstellen, d.h. auf Intervall [1000,2000] begrenzen
		if (U0 > 2000) U0 = 2000;
		if (U0 < 1000) U0 = 1000;
		if (U1 > 2000) U1 = 2000;
		if (U1 < 1000) U1 = 1000;

		iMicros[0] = (int) U0;
		iMicros[1] = (int) U1;
	}

	// Signallängen an Roboter senden => Aktualisiere die Geschwindigkeiten der Motoren
	Interface.SetOutputs(iMicros);
}



