/*
 * main.cpp
 *
 *  Created on: 15.03.2019
 *  Author: sefi16
 */
#include<iostream>
#include <pigpio.h>
#include <ncurses/curses.h>
#include "KeyboardControl.h"
#include "PIDController.h"
#include "PosEstimation.h"
#include "Maneuver.h"
#include "RobotControl.h"


void Manoeversteuerung() {
	RobotControl RC = RobotControl();
	do {
		RC.Communicate();
	} while (RC.isActive());
}


void Tastatursteuerung() {
	KeyboardControl KC = KeyboardControl();
	KC.Communicate();
}


bool SteuerungsAuswahl() {

	char cEingabe = '\0';
	int iAuswahl = -1;

	// Startet <ncurses> und initialisieren
	initscr();
	nodelay(stdscr, TRUE);
	noecho();

	do {
		printw("Wählen Sie eine Option:\n  (1) Tastatursteuerung     (2) Manöversteuerung    (q) Verlassen/Beenden\n\nIhre Auswahl: ");
		cEingabe = getch();
		if (cEingabe != -1) {
			clear();
			switch (cEingabe) {
				case '1':
					iAuswahl = 1;
					break;
				case '2':
					iAuswahl = 2;
					break;
				default:
					break;
			}
		}
	} while (cEingabe != 'q' && cEingabe != '1' && cEingabe != '2');

	endwin(); // ncurses beenden

	if (iAuswahl < 0) return false; // 'q' wurde gedrückt

	if (iAuswahl == 1) Tastatursteuerung();
	else if (iAuswahl == 2) Manoeversteuerung();

	return true;
}


int main() {

	// Initialisiere die Bibliothek vor dem ersten Aufruf
	gpioInitialise();

	bool bRetry = false;

	//do {
		bRetry = SteuerungsAuswahl();
	//} while (bRetry);


	// Anhalten der PIGPIO-Bibliothek
	gpioTerminate();

	return 0;
}
