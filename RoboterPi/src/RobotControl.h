/*
 * RobotControl.h
 *
 *  Created on: 14.03.2019
 *  Author: sefi16
 */

#ifndef ROBOTCONTROL_H
#define ROBOTCONTROL_H

//#include "InterfaceSIM.h"
#include "InterfaceHW.h"
#include "Maneuver.h"
#include "PosEstimation.h"
#include "PIDController.h"

class RobotControl {

	const double Kp = 500.0;
	const double Ki = 1850.0;
	const double Kd = 0.0;
	const double timestep = 0.04;
	const int iTimer = 0; // Wert zwischen 0 und 9

	private:
		//InterfaceSIM Interface;
		InterfaceHW Interface;
		Maneuver m_Maneuver;
		PosEstimation posEstimation;
		PIDController motorR;
		PIDController motorL;
		bool bIsActive;
		int * iMicros; // ServoSignallänge für Motoren
		double adCurrentSpeed[2];
	public:
		RobotControl();
		~RobotControl();
		static RobotControl * transferPointer;
		static void transferFunction(void);
		bool isActive();
		void Step();
		void Communicate();

};



#endif /* ROBOTCONTROL_H */
