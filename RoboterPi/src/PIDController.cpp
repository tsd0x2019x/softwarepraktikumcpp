/*
 * PIDController.cpp
 *
 *  Created on: 14.03.2019
 *  Author: sefi16
 */
#include "PIDController.h"

PIDController::PIDController(double Kp, double Ki, double Kd, double Ta) {
	// Initialisiere die Hilfsvariablen für die Berechnung der Stellgröße mit 0
	dEsum = 0; deold = 0; dU = 0;
	// Zuweisung der Verstärkungsfaktoren und Abtastzeit
	dKp = Kp; dKi = Ki; dKd = Kd; dTa = Ta;
}

PIDController::~PIDController() {}

/*
 * @params:
 * 		dW : Führungsgröße (y_soll)
 * 		dY : Regelgröße (y_ist)
 */
void PIDController::CalculateU(/* Sollwert */ double dW, /* Istwert */ double dY) {
	// Regelabweichung zum Zeitpunkt k
	double denew = dW - dY;
	// Summe aller Regelabweichungen bis zum Zeitpunkt k
	dEsum += denew;
	// Berechnung der Stellgroesse U zum Zeitpunkt k
	dU = dKp * denew + dKi * dTa * dEsum + dKd * (denew - deold) / dTa;
	// Aktualisiere Regelabweichung
	deold = denew;
}


double PIDController::GetU() {
	return dU;
}

void PIDController::SetFactors(double Kp, double Ki, double Kd) {
	dKp = 0; dKi = 0; dKd = 0;
}
