/*
 * KeyboardControl.h
 *
 *  Created on: 13.03.2019
 *  Author: sefi16
 */

#ifndef KEYBOARDCONTROL_H
#define KEYBOARDCONTROL_H

#define SPEED_MAX 0.500000
#define SPEED_MIN -0.500000

//#include "InterfaceSIM.h"
#include "InterfaceHW.h"
#include "PIDController.h"

//#define M_KP 500.0
//#define M_KI 1850.0
//#define M_KD 0.0
//#define M_TA 0.04  // Zeitschrittlänge, timestep [s]

class KeyboardControl {

	const double M_KP = 500.0;
	const double M_KI = 1850.0;
	const double M_KD = 0.0;
	const double M_TA = 0.04;  // timestep [s], Zeitschrittlänge
	const int iTimer = 0; // Wert zwischen 0 und 9

	private:
		//InterfaceSIM m_ISIM;
		InterfaceHW m_IHW;
		int * iMicros;
		/* Die Arrays speichern an der Stelle 0 die Geschwindigkeit des rechten und an der Stelle 1 die Geschwindigkeit des linken Motors */
		double * m_speedSoll; // Wunschgeschwindigkeit
		double * m_speedIst; // Aktuelle Geschwindigkeit
		PIDController * pidMotor0; // rechter Motor
		PIDController * pidMotor1; // linker Motor
		void StopMotors();

	public:
		KeyboardControl();
		~KeyboardControl();
		static KeyboardControl * transferPointer;
		static void transferFunction(void);
		void Communicate();
		void Step();

};


#endif /* KEYBOARDCONTROL_H */
