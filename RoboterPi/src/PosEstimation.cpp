/*
 * PosEstimation.cpp
 *
 *  Created on: 14.03.2019
 *  Author: sefi16
 */
#include "PosEstimation.h"

PosEstimation::PosEstimation() : x(new double[3]), vAvarage(0) {
	Reset();
}

PosEstimation::~PosEstimation() {
	delete[] x;
}

void PosEstimation::Reset() {
	x[0] = 0; x[1] = 0; x[2] = 0; vAvarage = 0;
}

/*
 * Berechnet die aktuelle Position (x_k, y_k) und Raumrichtung (w_k)
 */
void PosEstimation::PredictPosition(/* vrechts */ double dSpeedR, /* vlinks */ double dSpeedL, /* Zeitschrittslänge */ double dTimestep) {
	// Berechne die aktuelle Position (x_k, y_k), mithilfe von Rückwärts-Euler-Verfahren, aus den vorherigen berechneten Positionen (x_{k-1}, y_{k-1}).
	// (Vorherige) Positionen sind in x[0], x[1] gespeichert. Mittlere Geschwindigkeit ist vAverage.
	x[0] += vAvarage * dTimestep * cos(x[2]);
	x[1] += vAvarage * dTimestep * sin(x[2]);
	// Winkelgeschwindigkeit
	double w = (dSpeedR - dSpeedL) / 0.23; // 0.23 ist der Abstand zwischen den Rädern des Roboters
	// Berechne die aktuelle Richtung (Winkel) w_k, in die sich der Roboter bewegt. In x[2] ist die vorherige Richtung (Winkel) gespeichert.
	w = x[2] + dTimestep * w;
	// Winkel w_k ist im Einheitskreis ]-PI,PI] begrenzt
	w = fmod(w, 2 * M_PI);
	if (w > M_PI) w -= 2 * M_PI;
	if (w <= -M_PI) w += 2 * M_PI;
	// Winkel w_k in Array speichern
	x[2] = w;
	// Aktualisiere die mittlere Geschwindigkeit
	vAvarage = (dSpeedR + dSpeedL) / 2;
}

double * PosEstimation::GetPosition() {
	return x; // &x[0]
}



