/*
 * Maneuver.cpp
 *
 *  Created on: 14.03.2019
 *  Author: sefi16
 */
#include <cmath>
#include <fstream>
#include <cmath>
#include "Maneuver.h"

Maneuver::Maneuver() : bIsRunning(false), /* Max Wunschgeschwindigkeit */ dMaxSpeed(0.5), /* Max Abweichung 0.02[m] */ dPosDifference(0.02) {
	adWishSpeed[0] = 0.0;
	adWishSpeed[1] = 0.0;
}

Maneuver::~Maneuver() {}

void Maneuver::CalcCircle(double dRadius, double dSpeed, double dTimestep) {
	CoordList.clear();
	double dX = 0, dY = 0;
	for(int counter = 1; counter < (int)((2*M_PI)/((dSpeed/dRadius)*dTimestep)); counter++) {
		dX = dRadius * sin(counter * (dSpeed/dRadius) * dTimestep);
		dY = dRadius * (1 - cos(counter * (dSpeed/dRadius) * dTimestep));
		CoordList.push_back(Coord(dX,dY,dSpeed));
	}
}

void Maneuver::CalcEight(double dRadius, double dSpeed, double dTimestep) {
	CoordList.clear();
	double dX = 0, dY = 0;
	// 1. Halbkreis
	for(int counter = 1; counter < (int)((2*M_PI)/((dSpeed/dRadius)*dTimestep)); counter++) {
		dX = dRadius * sin(counter * (dSpeed/dRadius) * dTimestep);
		dY = dRadius * (1 - cos(counter * (dSpeed/dRadius) * dTimestep));
		CoordList.push_back(Coord(dX,dY,dSpeed));
	}
	// 2. Halbkreis
	for(int counter = 1; counter < (int)((2*M_PI)/((dSpeed/dRadius)*dTimestep)); counter++) {
		dX = dRadius * sin(counter * (dSpeed/dRadius) * dTimestep);
		dY = (-dRadius) * (1 - cos(counter * (dSpeed/dRadius) * dTimestep));
		CoordList.push_back(Coord(dX,dY,dSpeed));
	}
}

void Maneuver::LogList(std::string sDatei) {
	std::ofstream oFile;
	oFile.open(sDatei.c_str(), std::ios::out | std::ios::ate | std::ios:: app); // Setzt Cursor ans Dateiende (ate) und Text am Ende anhängen (app)
	for(iter = CoordList.begin(); iter != CoordList.end(); iter++) {
		oFile << iter->x << "\t" << iter->y << std::endl;
	}
	oFile.close();
}

bool Maneuver::isRunning() {
	return bIsRunning;
}

void Maneuver::Start() {
	iter = CoordList.begin();
	bIsRunning = true;
}

void Maneuver::Stop() {
	bIsRunning = false;
}

void Maneuver::Proceed() {
	bIsRunning = true;
}

void Maneuver::CalcManeuverSpeed(double dX, double dY, double dW) {
	if (fabs(iter->x - dX) <= dPosDifference && fabs(iter->y - dY) <= dPosDifference) iter++;
	if (iter == CoordList.end()) {
		adWishSpeed[0] = 0.0;
		adWishSpeed[1] = 0.0;
		Stop();
	}
	else {
		// Winkel zwischen Soll- und Istposition berechnen
		double phi = atan2(iter->y - dY, iter->x - dX); // liefert Winkelwert im Bereich [-PI,PI]
		// Winkeldifferenz berechnen
		double difPhi = phi - dW;
		// Winkeldifferenz auf ]-PI,PI] begrenzen
		if (difPhi <= -M_PI) difPhi += 2*M_PI;
		if (difPhi > M_PI) difPhi -= 2*M_PI;
		// Rotationsanteil bestimmen
		double dRot = 2 * difPhi;
		if (fabs(dRot) > 0.5) dRot = (dRot < 0) ? -0.5 : 0.5;
		// Translationsanteil bestimmen
		double dTra = iter->v;
		// Geschwindigkeitsanteile überprüfen
		double dSpeed0 = dTra + dRot;
		double dSpeed1 = dTra - dRot;
		if (dTra * dRot > 0.0) {
			if (dSpeed0 > dMaxSpeed) {
				dTra = dMaxSpeed - dRot; // Translationsanteil verringern
			}
			else if (dSpeed0 < -dMaxSpeed) {
				dTra = -dMaxSpeed - dRot; // Translationsanteil erhöhen
			}
		} else if (dTra * dRot < 0.0) {
			if (dSpeed1 > dMaxSpeed) {
				dTra = dMaxSpeed + dRot; // Translationsanteil verringern
			}
			else if (dSpeed1 < -dMaxSpeed) {
				dTra = -dMaxSpeed + dRot; // Translationsanteil erhöhen
			}
		}
		// Geschwindigkeitsanteile summieren
		adWishSpeed[0] = dTra + dRot; // rechter Motor
		adWishSpeed[1] = dTra - dRot; // linker Motor
	}
}

double * Maneuver::GetManeuverSpeed() {
	return adWishSpeed;
}
