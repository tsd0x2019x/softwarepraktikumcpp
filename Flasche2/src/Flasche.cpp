/*
 * Flasche.cpp
 *
 *  Created on: 12.03.2019
 *  Author: sefi16
 */
#include <iostream>
#include <string>

#include "Flasche.h"

Flasche::Flasche() : dVolumen(0), sMaterial("")
{}

Flasche::~Flasche()
{}

double Flasche::getVolumen()
{
	return dVolumen;
}

std::string Flasche::getMaterial()
{
	return sMaterial;
}

void Flasche::setVolumen(double volumen)
{
	dVolumen = volumen;
}

void Flasche::setMaterial(std::string material)
{
	sMaterial = material;
}

void Flasche::printFlasche()
{
	std::cout << "=> Volumen: " << dVolumen << std::endl;
	std::cout << "=> Material: " << sMaterial << std::endl;
}

void Flasche::adoptFlasche(Flasche flasche)
{
	dVolumen = flasche.dVolumen;
	sMaterial = flasche.sMaterial;
}


