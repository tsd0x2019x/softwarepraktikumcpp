/*
 * Flasche.h
 *
 *  Created on: 12.03.2019
 *  Author: sefi16
 */

#ifndef FLASCHE_H
#define FLASCHE_H

#include <string>

class Flasche
{
	private:
		double dVolumen;
		std::string sMaterial;

	public:
		Flasche();
		~Flasche();
		double getVolumen();
		std::string getMaterial();
		void setVolumen(double volumen);
		void setMaterial(std::string material);
		void printFlasche();
		void adoptFlasche(Flasche flasche);
};

#endif /* FLASCHE_H */
