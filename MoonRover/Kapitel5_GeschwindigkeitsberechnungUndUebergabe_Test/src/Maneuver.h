/*
 * Maneuver.h
 *
 *  Created on: Aug 25, 2014
 *      Author: pi
 */

#ifndef MANEUVER_H_
#define MANEUVER_H_

#include <cmath>
#include <list>
#include <fstream>

class Maneuver
{
	public:
		//Standardkonstruktor
		Maneuver();

		//Destruktor
		virtual ~Maneuver();

		/*
		Methode zum Initialisieren der Manoever (Kreis und Acht)
		Parameter:	dRadius     Radius des Manoevers										[m]
					dSpeed      Geschwindigkeit mit der Manoever durchfahren werden soll	[m/s]
		*/
		void CalcCircle (double dRadius, double dSpeed, double dTimestep);
		void CalcEight (double dRadius, double dSpeed, double dTimestep);

		/*
		Methode zum Schreiben der x- und y-Kordinaten in eine Log-Datei
		*/
		void LogList();

		/*
        Methode zur Uebergabe des Flags _isRunning
		return:		Wert des Flags _isRunning
        */
        bool isRunning();

        //Methode zum Starten des Manoevers
        void Start(void);

        //Methode zum Unterbrechen des Manoevers
        void Stop(void);

        //Methode zum Starten des Manoevers
        void Proceed(void);

        /*
        Methode zur Berechnung des Rotationsanteils _dRot der Soll-Geschwindigkeiten
        Parameter:	dXakt  Ortsschaetzung der x-Koordinate
                    dYakt  Ortsschaetzung der y-Koordinate
                    dWakt  Ortsschaetzung der Raumrichtung
        */
        void CalcRotation(double dXakt, double dYakt, double dWakt);

		//Variable fuer Rotationsanteil der Soll-Geschwindigkeit [m/s]
		double _dRot;

        /*
        Methode zur Berechnung des Translationsanteils _dTra der Soll-Geschwindigkeiten
        */
        void CalcTranslation();

		//Variable fuer Translationsanteil der Soll-Geschwindigkeit [m/s]
		double _dTra;

        /*
        Methode zur Berechnung der Soll-Geschwindigkeiten zum Abfahren der Manoever (Kreis und Acht)
        Parameter:	dXakt  Ortsschaetzung der x-Koordinate
                    dYakt  Ortsschaetzung der y-Koordinate
                    dWakt  Ortsschaetzung der Raumrichtung
        */
        void CalcManeuverSpeed(double dXakt, double dYakt, double dWakt);

		/*
		Methode zur Uebergabe der gerechneten Geschwindigkeiten fuer die Manoeverfahrt
		return:     Zeiger auf Geschwindigkeitsarray fuer rechts und links
		*/
		double* GetManeuverSpeed();

	private:
		//Struktur fuer Koordinatenliste zur Manoeverbeschreibung
		struct _Coord{
					  double _dX;
					  double _dY;
					  double _dV;
					};

		//Koordinaten-Liste erstellen
		std::list<_Coord> _CoordList;
		//Iterator fuer die Liste erstelllen
		std::list<_Coord>::iterator _iter;

		//Speicher zur Abfrage ob Liste gefahren wird deklarieren
        bool _isRunning;

        //Array fuer Geschwindigkeit rechts und links
        //erstes Element rechter Motor
        //zweites Element linker Motor
        double _adWishSpeed[2];

		//Variable fuer maximale Geschwindigkeit [m/s]
		double _dMaxSpeed;

		//Variable fuer Ortsabweichung vom berechneten Punkt [m]
		double _dPosDifference;
};

#endif /* MANEUVER_H_ */
