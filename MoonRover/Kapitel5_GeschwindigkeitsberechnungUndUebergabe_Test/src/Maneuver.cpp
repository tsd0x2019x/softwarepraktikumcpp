/*
 * Maneuver.cpp
 *
 *  Created on: Aug 25, 2014
 *      Author: pi
 */

#include "Maneuver.h"

//Standartkonstruktor
Maneuver::Maneuver()
{
    //Speicher zur Abfrage ob Liste gefahren wird initialisieren
    _isRunning = false;

    //Array fuer Geschwindigkeit rechts und links
    //erstes Element rechter Motor
    //zweites Element linker Motor
    _adWishSpeed[0] = 0.0;
    _adWishSpeed[1] = 0.0;

	//Variable fuer maximale Geschwindigkeit initialisieren [m/s]
	_dMaxSpeed = 0.5;

	//Variable fuer Rotationsanteil der Soll-Geschwindigkeit [m/s]
	_dRot = 0.0;

	//Variable fuer Translationsanteil der Soll-Geschwindigkeit [m/s]
	_dTra = 0.0;

	//Variable fuer Ortsabweichung vom berechneten Punkt initialisieren [m]
	_dPosDifference = 0.02;
}

//Destruktor
Maneuver::~Maneuver() {
	// TODO Auto-generated destructor stub
}

/*
Methode zum Initialisieren eines Kreises
Parameter:	dRadius     Radius des Manoevers										[m]
			dSpeed      Geschwindigkeit mit der Manoever durchfahren werden soll	[m/s]
*/
void Maneuver::CalcCircle (double dRadius, double dSpeed, double dTimestep)
{
    //aktuelle Liste loeschen
	_CoordList.clear();

    //Liste mit Kreis initialisieren
    //lokale Hilfsvariable vom Typ der Koordinaten Struktur zum Fuellen der Liste
    _Coord help;

    //for-Schleife erzeugt fuer jeden Zeitschritt der Steuerung ein neues Listenelement
    for (int counter=1; counter < ( (int) (2 * M_PI * dRadius / (dSpeed * dTimestep)) ); counter++)
    {
        //x-Koordinate fuer einen Kreis tangential an x-Achse durch en Ursprung berechnen
        help._dX = dRadius *         sin(counter * ((dSpeed * dTimestep) / dRadius) );
        //y-Koordinate fuer einen Kreis tangential an x-Achse durch en Ursprung berechnen
        help._dY = dRadius * (1.0 -  cos(counter * ((dSpeed * dTimestep) / dRadius) ) );
        //Geschwindigkeit uebergeben
        help._dV = dSpeed;
        //Listenelement hinten an Liste anhaengen
        _CoordList.push_back(help);
    }
}

/*
Methode zum Initialisieren einer Acht)
Parameter:	dRadius     Radius des Manoevers										[m]
			dSpeed      Geschwindigkeit mit der Manoever durchfahren werden soll	[m/s]
*/
void Maneuver::CalcEight (double dRadius, double dSpeed, double dTimestep)
{
    //aktuelle Liste loeschen
    _CoordList.clear();

    //Liste mit Achter initialisieren
    //lokale Hilfsvariable vom Typ der Koordinaten Struktur zum Fuellen der Liste
    _Coord help;
    //erster Teilkreis
    //for-Schleife erzeugt fuer jeden Zeitschritt der Steuerung ein neues Listenelement
    for (int counter=1; counter< ( (int) (2 * M_PI * dRadius / (dSpeed * dTimestep)) ); counter++)
    {
        //x-Koordinate fuer einen Kreis tangential an x-Achse durch en Ursprung berechnen
        help._dX = dRadius *         sin(counter * ((dSpeed * dTimestep) / dRadius) );
        //y-Koordinate fuer einen Kreis tangential an x-Achse durch en Ursprung berechnen
        help._dY = dRadius * (1.0 -  cos(counter * ((dSpeed * dTimestep) / dRadius) ) );
        //Geschwindigkeit uebergeben
        help._dV = dSpeed;
        //Listenelement hinten an Liste anhaengen
        _CoordList.push_back(help);
    }

    //zweiter Teilkreis
    //for-Schleife erzeugt fuer jeden Zeitschritt der Steuerung ein neues Listenelement
    for (int counter=1; counter< ( (int) (2 * M_PI * dRadius / (dSpeed * dTimestep)) ); counter++)
    {
        //x-Koordinate fuer einen Kreis tangential an x-Achse durch en Ursprung berechnen
        help._dX = dRadius *          sin(counter * ((dSpeed * dTimestep) / dRadius) );
        //y-Koordinate fuer einen Kreis tangential an x-Achse durch en Ursprung berechnen
        help._dY = -dRadius * (1.0 -  cos(counter * ((dSpeed * dTimestep) / dRadius) ) );
        //Geschwindigkeit uebergeben
        help._dV = dSpeed;
        //Listenelement hinten an Liste anhaengen
        _CoordList.push_back(help);
    }
}

/*
Methode zum Schreiben der x- und y-KKordinaten in eine Log-Datei
*/
void Maneuver::LogList()
{
	std::ofstream _mystream;
	_mystream.open("LogFile.txt", std::ios::out | std::ios::trunc);
	for(std::list<_Coord>::iterator _myit = _CoordList.begin(); _myit != _CoordList.end(); _myit++)
	{
		_mystream << (*_myit)._dX << '\t' << (*_myit)._dY << '\n';
	}
	_mystream.close();
}

/*
Methode zur Uebergabe des Flags _isRunning
return:		Wert des Flags _isRunning
*/
bool Maneuver::isRunning()
{
	//Flag _isRunning zurueckgeben
	return _isRunning;
}

//Methode zum Starten des Manoevers
void Maneuver::Start()
{
    //Iterator auf erstes Listenelement biegen
    _iter = _CoordList.begin();
    //Variable zur Erkennung der Manoeverfahrt auf 1 (ein) setzen
    _isRunning = true;
}

//Methode zum Stoppen des Manoevers
void Maneuver::Stop()
{
    //Variable zum Unterbrechen der Manoeverfahrt auf 1 (unterbrechen) setzen
    _isRunning = false;
}

//Methode zum Neustarten des Manoevers
void Maneuver::Proceed()
{
    //Variable zum Unterbrechen der Manoeverfahrt auf 0 (nicht unterbrechen) setzen
    _isRunning = true;
}

/*
Methode zur Berechnung des Rotationsanteils _dRot der Soll-Geschwindigkeiten
Parameter:	dXakt  Ortsschaetzung der x-Koordinate
            dYakt  Ortsschaetzung der y-Koordinate
            dWakt  Ortsschaetzung der Raumrichtung
*/
void Maneuver::CalcRotation(double dXakt, double dYakt, double dWakt)
{
    //lokale Variablen fuer Winkel
    double dPhi=0.0;
    double dDeltaPhi=0.0;

	//Winkel zwischen Ist-Position und Ziel-Postition berechnen
	//phi = arctan( (Y_soll-Y_ist) / (X_soll-X_ist) )
	dPhi = atan2((_iter->_dY - dYakt) , (_iter->_dX - dXakt));

	//Winkeldifferenz zwischen Richtung zur Ziel-Position und Ausrichtung des Roboters berechnen
	dDeltaPhi = dPhi - dWakt;
	//Winkeldifferenz auf ]-PI, PI] begrenzen, um Rotation auf maximal 180° zu begrenzen
	if (dDeltaPhi >   M_PI)   dDeltaPhi = dDeltaPhi - (2.0*M_PI);
	else if (dDeltaPhi <= -M_PI)   dDeltaPhi = dDeltaPhi + (2.0*M_PI);

	//Soll-Rotationsanteil der Geschwindigkeit berechnen und uebergeben
	//_dRot [m/s] = 2.0 [m/(s*rad)] * fDeltaPhi [rad]
	_dRot = 2.0 * dDeltaPhi;

	//Wertebereich des Rotationsanteil auf maximalen Geschwindigkeitsbetrag eingrenzen
	if(_dRot > _dMaxSpeed)     _dRot = _dMaxSpeed;
	if(_dRot <-_dMaxSpeed)     _dRot =-_dMaxSpeed;
}

/*
Methode zur Berechnung des Translationsanteils _dTra der Soll-Geschwindigkeiten
*/
void Maneuver::CalcTranslation()
{
	//Translationsanteil der Geschwindigkeit aus Liste übergeben
	_dTra = _iter->_dV;

	//Wenn |Rotationanteil| + |Translationanteil| groesser als max. Geschwindigkeitsbetrag,
	//Translationsanteil reduzieren
	if((_dTra*_dRot)>0.0)
	{
		if((_dTra + _dRot) > _dMaxSpeed)		_dTra =	 _dMaxSpeed-_dRot;
		else if((_dTra + _dRot) < -_dMaxSpeed)	_dTra = -_dMaxSpeed-_dRot;
	}
	else if((_dTra*_dRot)<0.0)
	{
		if((_dTra - _dRot) > _dMaxSpeed)		_dTra =  _dMaxSpeed+_dRot;
		else if((_dTra - _dRot) < -_dMaxSpeed)	_dTra = -_dMaxSpeed+_dRot;
	}
}

/*
Methode zur Berechnung der Soll-Geschwindigkeiten zum Abfahren der Manoever (Kreis und Acht)
Parameter:	dXakt  Ortsschaetzung der x-Koordinate
            dYakt  Ortsschaetzung der y-Koordinate
            dWakt  Ortsschaetzung der Raumrichtung
*/
void Maneuver::CalcManeuverSpeed(double dXakt, double dYakt, double dWakt)
{
	//Wenn WunschKoordinaten mit vorgegebener Genauigkeit erreicht sind, naechsten Punkt in Liste auswaehlen
	if( (dXakt>=((_iter->_dX)-_dPosDifference) && dXakt<=((_iter->_dX)+_dPosDifference)) && (dYakt>=((_iter->_dY)-_dPosDifference) && dYakt<=((_iter->_dY)+_dPosDifference) ) )
	{
		//Iterator auf naechstes Elemente biegen
		_iter++;
	}

	//wenn Liste am Ende Roboter stoppen und Manoever beenden
	if(_iter == _CoordList.end())
	{
		//Geschwindigkeit auf null setzen
		_adWishSpeed[0] = 0.0;
		_adWishSpeed[1] = 0.0;

		//Fahrt beenden
		Stop();
	}
	else
	{
		//Rotationsanteil berechnen
		CalcRotation(dXakt, dYakt, dWakt);

		//Translationsanteil berechnen
		CalcTranslation();

		//Geschwindigkeit aus Rotation und Translation zusammensetzten
		//Bei (mathematisch) positivem Drehsinn muss Geschwindigkei rechts groeßer sein als links
		//--> Rotationsanteil rechts addieren und links subtraieren
		_adWishSpeed[0] = _dTra + _dRot; //erstes Element rechter Motor
		_adWishSpeed[1] = _dTra - _dRot; //zweites Element linker Motor
	}
}

/*
Methode zur Uebergabe der gerechneten Geschwindigkeiten fuer die Manoeverfahrt
return:     Zeiger auf Geschwindigkeitsarray fuer rechts und links
*/
double* Maneuver::GetManeuverSpeed()
{
    //Zeiger auf Array mit Geschwindigkeiten fuer rechten und linken Motor ubergeben
    return _adWishSpeed;
}
