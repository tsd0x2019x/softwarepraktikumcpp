/*
 * main.cpp
 *
 *  Created on: Sep 1, 2014
 *      Author: pi
 */

#include "Maneuver.h"
#include <iostream>
#include <fstream>

using namespace std;

int main(void)
{
	//Streams zum Einlesen der Werte erzeugen
	ifstream InputFile;
	ifstream VglFile;

	//Entsprechende Dateien oeffnen
	InputFile.open("ManeuverSpeedInput.txt", ios::in);
	VglFile.open("ManeuverSpeedVgl.txt", ios::in);
	//Variablen fuer Inputwerte erzeugen
	double dInputX = 0.0, dInputY = 0.0, dInputW = 0.0;

	//Variablen fuer Vergleichswerte erzeugen
	double dVgl1 = 0.0, dVgl2 = 0.0;

	//Maneuver-Objekt erstellen
	Maneuver _Maneuver = Maneuver();

	//Kreis initialisieren
	_Maneuver.CalcCircle(2.0, 0.3, 0.04);

	//Maneuver "starten" --> Iterator auf erstes Element biegen
	_Maneuver.Start();

	while(!InputFile.eof())
	{
		//Werte aus Inputfile einlesen
		InputFile >> dInputX;
		InputFile >> dInputY;
		InputFile >> dInputW;

		//Soll-Geschwindigkeiten berechnen
		_Maneuver.CalcManeuverSpeed(dInputX, dInputY, dInputW);

		//Berechnete Soll-Geschwindigkeiten ausgeben
		cout 	<< "Berechnet:      "
				<< *(_Maneuver.GetManeuverSpeed()) 		<< "\t"
				<< *(_Maneuver.GetManeuverSpeed()+1)	<< endl;

		//Vergleichswerte einlesen
		VglFile >> dVgl1;
		VglFile >> dVgl2;

		//Vergleichswerte ausgeben
		cout << "Vergleichswert: " << dVgl1 << "\t" << dVgl2 << endl << endl;
	}

	//Dateien schliessen
	InputFile.close();
	VglFile.close();

	return 0;
}

