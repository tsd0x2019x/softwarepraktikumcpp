//============================================================================
// Name        : Heron.cpp
// Author      : sefi16
// Version     :
// Copyright   : Your copyright notice
// Description : Heron Verfahren in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

float heron_ite(/* Rootzahl */ int a, /* Startwert*/ float x0) {
	if ( a == 0 ) return 0;

	int ite_num = 10; // Number of iterations
	float xn = x0;

	while ( ite_num > 0 ) {
		xn = xn + (a / xn);
		xn /= 2;
		ite_num -= 1;
	}

	return xn;
}

int main() {
	cout << "*** Heron Verfahren ***" << endl; // prints !!!Hello World!!!

	int iNumber = 0;
	float fWurzel = 0;

	cout << "Bitte eine positive ganze Zahl eingeben" << endl;
	cin >> iNumber;

	while (cin.fail() || iNumber < 0) {
		cout << "ERROR::Falscheingabe: Bitte eine positive ganze Zahl eingeben" << endl;
		cin.clear();
		cin.ignore(200, '\n');
		cin >> iNumber;
	}

	fWurzel = heron_ite(iNumber, 1);

	cout << "Wurzel(" << iNumber << ") = " << fWurzel << endl;

	return 0;
}
