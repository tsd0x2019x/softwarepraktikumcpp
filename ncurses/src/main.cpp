/*
 * beispiel.cpp
 *
 *  Created on: 13.03.2019
 *  Author: sefi16
 */
#include <iostream>
#include <list>
#include "ncurses.h"

int main()
{
	std::list<char> lst;
	char ch = 'c';

	// Start ncurses
	initscr();
	nodelay(stdscr, TRUE);
	noecho();

	while (ch != 'q') {
		ch = getch();
		if (ch != -1) {
			lst.push_front(ch);
			clear();
			printw("Eingabe ist: %c", ch);
		}
	}

	endwin(); // End ncurses

	std::cout << std::endl << "Vom Benutzer eingegeben: ";

	for(std::list<char>::iterator mIte = lst.begin(); mIte != lst.end(); mIte++) {
		if (*mIte != 'q') {
			std::cout << *mIte;
		}
	}

	lst.clear();

	std::cout << std::endl;

	return 0;
}


