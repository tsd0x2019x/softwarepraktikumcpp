/*
 * Date.cpp
 *
 *  Created on: 12.03.2019
 *  Author: sefi16
 */
#include <iostream>
#include <cstdlib>
#include <ctime>

#include "Date.h"

Date::Date() /* Standardkonstruktor */
{
	srand(time(NULL)); // Einmalige Initialisierung des random-seeds
	m_year = rand() % 61 + 1971; // Jahr 1970-2030
	m_month = rand() % 12 + 1; // Monat 1-12
	m_day = (m_month == 2) ? 28 : (m_month % 2 == 1) ? 31 : 30;
}

Date::Date(int day, int month, int year)
{
	m_day = day; m_month = month; m_year = year;
}

Date::~Date()
{}

int Date::getDay() { return m_day; }
int Date::getMonth() { return m_month; }
int Date::getYear() { return m_year; }
bool Date::isEqual(Date dd) {
	if (m_day==dd.m_day && m_month==dd.m_month && m_year==dd.m_year)
		return true;
	return false;
}

int Date::compare(Date dd)
{
	if (dd.m_year < m_year) return 1; // vor
	else if (dd.m_year > m_year) return -1; // nach
	else // dd.m_year == m_year
	{
		if (dd.m_month < m_month) return 1; // vor
		else if (dd.m_month > m_month) return -1; // nach
		else // dd.m_month == m_month
		{
			if (dd.m_day < m_day) return 1; // vor
			else if (dd.m_day > m_day) return -1; // nach
			else
			{
				return 0; // gleich
			}
		}
	}
}

void Date::printDate(std::string name)
{
	std::cout << "Datum " << name << ": " << m_day << "." << m_month << "." << m_year << std::endl;
}

void Date::printDate()
{
	printDate("");
}
