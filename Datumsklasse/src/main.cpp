/*
 * main.cpp
 *
 *  Created on: 12.03.2019
 *  Author: sefi16
 */
#include <iostream>
#include <string>

#include "Date.h"

void printCompare(Date d1, std::string d1_name, Date d2, std::string d2_name) {
	if (d1.compare(d2) == 1) {
		std::cout << d2_name << " VOR " << d1_name << std::endl;;
	}
	else if (d1.compare(d2) == -1) {
		std::cout << d2_name << " NACH " << d1_name << std::endl;;
	}
	else {
		std::cout << d2_name << " GLEICH " << d1_name << std::endl;;
	}

}

int main() {
	Date d1 = Date(2,2,2011);
	//d1.m_day = 15;
	d1.printDate("1");

	Date d2 = Date(15,2,2011);
	//bool tf = d2.isEqual(d1);
	d2.printDate("2");

	Date d3 = Date();
	Date d4 = Date(2,2,2011);

	d3.printDate("3");
	d4.printDate("4");

	Date d5 = Date();
	d5.printDate("5");

	std::cout << std::endl;

	printCompare(d1,"Datum 1", d2, "Datum 2");
	printCompare(d1,"Datum 1", d3, "Datum 3");
	printCompare(d1,"Datum 1", d4, "Datum 4");
	printCompare(d1,"Datum 1", d5, "Datum 5");
	printCompare(d2,"Datum 2", d3, "Datum 3");
	printCompare(d2,"Datum 2", d4, "Datum 4");
	printCompare(d2,"Datum 2", d5, "Datum 5");
	printCompare(d3,"Datum 3", d4, "Datum 4");
	printCompare(d3,"Datum 3", d5, "Datum 5");
	printCompare(d4,"Datum 4", d5, "Datum 5");

}




