/*
 * Date.h
 *
 * Created on: 12.03.2019
 * Author: sefi16
 */
#ifndef DATE_H
#define DATE_H

#include <string>

class Date
{
	private:
		int m_day, m_month, m_year;
		//Date(int day, int month, int year);

	public:
		Date();
		Date(int day, int month, int year);
		~Date();
		bool isEqual(Date dd);
		int getDay();
		int getMonth();
		int getYear();
		int compare(Date date);
		void printDate();
		void printDate(std::string name);
};

#endif /* DATE_H */
